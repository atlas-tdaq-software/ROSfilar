/************************************************************************/
/*									*/
/* File: filar_driver.h							*/
/*									*/
/* This is the private header file for FILAR driver			*/
/*									*/
/* 25. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2005 - The software with that certain something *********/

#ifndef _FILAR_DRIVER_H 
#define _FILAR_DRIVER_H

#include <linux/types.h>
#include "filar_common.h"

#ifdef DMA_PROTOCOL
  #define MINREV 43  //Oldest supported revision
#else
  #define MINREV 32  //Oldest supported revision
#endif

#define BAR0     0
#define MAXIRQ   256


/********/
/*Macros*/
/********/
#ifdef DRIVER_DEBUG
  #define kdebug(x) {if (debug) printk x;}
#else
  #define kdebug(x)
#endif

#ifdef DRIVER_ERROR
  #define kerror(x) {if (errorlog) printk x;}
#else
  #define kerror(x)
#endif


/******************/
/*Type definitions*/
/******************/
typedef struct
{
  u_int ocr;            /*0x000*/
  u_int osr;            /*0x004*/
  u_int imask;          /*0x008*/
  u_int fifostat;       /*0x00c*/
  u_int scw;            /*0x010*/
  u_int ecw;            /*0x014*/
  u_int testin;         /*0x018*/
  u_int reqadd;         /*0x01c*/
  u_int blkctl;         /*0x020*/
  u_int ackadd;         /*0x024*/
  u_int efstatr;        /*0x028*/
  u_int efstata;        /*0x02c*/
  u_int d1[52];         /*0x030-0x0fc*/
  u_int req1;           /*0x100*/
  u_int ack1;           /*0x104*/
  u_int d2[2];          /*0x108-0x10c*/
  u_int req2;           /*0x110*/
  u_int ack2;           /*0x114*/
  u_int d3[2];          /*0x118-0x11c*/
  u_int req3;           /*0x120*/
  u_int ack3;           /*0x124*/
  u_int d4[2];          /*0x128-0x12c*/
  u_int req4;           /*0x130*/
  u_int ack4;           /*0x134*/
  u_int d5[34];         /*0x138-0x1bc*/
  u_int test_dma;       /*0x1c0*/
  u_int test_register;  /*0x1c4*/
} T_filar_regs;

typedef struct
{
  struct pci_dev *pciDevice;
  T_filar_regs *regs;
  u_char pci_revision;
  u_int pci_memaddr;          //MJ: Assumption: The BIOS will alwasy map the FILAR at a PCI address below 4GB
  u_char irq_line;
} T_filar_card;

struct filar_proc_data_t 
{
  char name[10];
  char value[100];
};

/***********/
/*Constants*/
/***********/
#define MAX_PROC_TEXT_SIZE 0x10000 //The output of "more /proc/filar" must not generate more characters than that
 
/******************************/
/*Standard function prototypes*/
/******************************/
static int filar_open(struct inode *ino, struct file *filep);
static int filar_release(struct inode *ino, struct file *filep);
static long filar_ioctl(struct file *file, u_int cmd, u_long arg);
static int filar_mmap(struct file *file, struct vm_area_struct *vma);
static ssize_t filar_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset);
int filar_proc_show(struct seq_file *sfile, void *p);

/*****************************/
/*Special function prototypes*/
/*****************************/
static int in_fifo_pop(int rol, int *data);
static int in_fifo_level(int rol, int *nfree);
static int hw_fifo_pop(int rol, int *data);
static int hw_fifo_push(int rol, int data);
static int out_fifo_push(int rol, int pciaddr, int fragsize, int fragstat, int scw, int ecw);
static int out_fifo_level(int rol, int *nfree);
static void blow_fuse(void);
static void read_ack_and_write_out(int card);
static void reset_cards(int mode);
static void flush_cards(void);
#endif
