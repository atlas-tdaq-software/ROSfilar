/************************************************************************/
/*									*/
/* File: filar_common.h							*/
/*									*/
/* This is the common public header file for FILAR library		*/
/* and driver								*/
/*									*/
/* 25. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2002 - The software with that certain something *********/

#ifndef _FILAR_COMMON_H 
#define _FILAR_COMMON_H

#ifdef __KERNEL__
  #include <linux/types.h>
  #define P_ID_FILAR 9   // Needs to be re-defined here since we do not want to include rcc_error.h at this level
#else
  #include <sys/types.h>
#endif

#define P_ID_FILAR_STR "FILAR library"

// Usewr modifyable constants
#define MAXCARDS         4    // Max. number of FILAR cards
#define MAXCHANNELS      4    // Max. number of channes per FILAR card
#define MAXROLS          (MAXCARDS * MAXCHANNELS)
#define MAXINFIFO        200  // MAX number of entries in the IN FIFO
#define MAXOUTFIFO       300  // MAX number of entries in the OUT FIFO
#ifdef DMA_PROTOCOL
  #define MAXHWFIFO      100  // MAX number of entries in the internal H/W FIFO.
                           // Has to be at least as deep as FILAR's H/W REQ/ACK
                           // FIFO plus one entry (which is immediately moved to the FILAR core logic)
#else
  #define MAXHWFIFO      30   // MAX number of entries in the internal H/W FIFO. 
#endif

// Internal constants (do not modify)
#define INFIFO_ELEMENTS  1
#define OUTFIFO_ELEMENTS 5

//Bits definitions used in the fragment status word
#define NO_SCW     0x00000001  //Start control word missing
#define NO_ECW     0x00000002  //End control word missing
#define BAD_SCW    0x00000004  //Unexpected start control word
#define BAD_ECW    0x00000008  //Unexpected end control word
#define SCW_DTE    0x00000010  //Data transmission error in SCW
#define SCW_CTE    0x00000020  //Control word transmission error in SCW
#define ECW_DTE    0x00000040  //Data transmission error in ECW
#define ECW_CTE    0x00000080  //Control word transmission error in ECW

/**********/
/* Macros */
/**********/
#define IN_INDEX(a, b)     (a * MAXINFIFO + b)
#define OUT_INDEX(a, b)    (a * MAXOUTFIFO * OUTFIFO_ELEMENTS + b * OUTFIFO_ELEMENTS)


/********************/
/* Type definitions */
/********************/
typedef struct 
{
  u_long physbase;
  u_int size;
  u_int insize;
  u_int outsize;
  u_int ptrsize;
} FILAR_open_t;

typedef struct 
{
  u_int enable[MAXROLS];
  u_int psize;
  u_int bswap;
  u_int wswap;
  u_int interrupt;
  u_int scw;
  u_int ecw;
  u_int ccw;
} FILAR_config_t;

typedef struct 
{
  u_int nchannels;
  u_int channels[MAXROLS];
  u_int enabled[MAXROLS];
} FILAR_info_t;

typedef struct 
{
  u_int nvalid;
  u_int channel; 
  u_int pciaddr[MAXINFIFO];
} FILAR_in_t;

typedef struct 
{
  u_int nvalid;  
  u_int pciaddr[MAXOUTFIFO];
  u_int fragsize[MAXOUTFIFO];
  u_int fragstat[MAXOUTFIFO];
  u_int scw[MAXOUTFIFO];
  u_int ecw[MAXOUTFIFO];
} FILAR_out_t;


/*************/
/*error codes*/
/*************/

/*error codes*/
enum
{
  FILAR_SUCCESS = 0,
  FILAR_NOTKNOWN = (P_ID_FILAR <<8)+1,
  FILAR_EFAULT,
  FILAR_EIO,
  FILAR_ENOSYS,
  FILAR_ILLREV,
  FILAR_IOREMAP,
  FILAR_REQIRQ,
  FILAR_FILE,
  FILAR_NOTOPEN,
  FILAR_NOCARD,
  FILAR_FIFOFULL,
  FILAR_ERROR_FAIL,
  FILAR_KMALLOC,
  FILAR_MMAP,
  FILAR_MUNMAP,
  FILAR_NOHW,
  FILAR_STUCK,
  FILAR_EREQ,
  FILAR_EACK,
  FILAR_USED,
  FILAR_ALIGN,
  FILAR_TOOMANYCARDS,
  FILAR_NO_CODE
};

#endif
