/************************************************************************/
/*									*/
/* File: filar.h							*/
/*									*/
/* This is the public header file for FILAR library			*/
/*									*/
/* 25. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2002 - The software with that certain something *********/

#ifndef _FILAR_H 
#define _FILAR_H

#include <linux/types.h>
#include "filar_common.h"

/*******/
/*Types*/
/*******/
typedef unsigned int   FILAR_ErrorCode_t;


/************/
/*Prototypes*/
/************/
#ifdef __cplusplus
extern "C" {
#endif


/*******************************/ 
/*Official functions of the API*/
/*******************************/ 
FILAR_ErrorCode_t FILAR_Open(void);
FILAR_ErrorCode_t FILAR_Close(void);
FILAR_ErrorCode_t FILAR_Info(FILAR_info_t *info);
FILAR_ErrorCode_t FILAR_Init(FILAR_config_t *config);
FILAR_ErrorCode_t FILAR_Reset(u_int card);
FILAR_ErrorCode_t FILAR_LinkReset(u_int card);
FILAR_ErrorCode_t FILAR_LinkStatus(u_int card, u_int *status);
FILAR_ErrorCode_t FILAR_PagesIn(FILAR_in_t *fifoin);
FILAR_ErrorCode_t FILAR_PagesOut(u_int channel, FILAR_out_t *fifoout);
FILAR_ErrorCode_t FILAR_Test(void);
FILAR_ErrorCode_t FILAR_InFree(u_int channel, u_int *nfree);
FILAR_ErrorCode_t FILAR_Flush(void);

/******************************/
/* Internal service functions */
/******************************/
FILAR_ErrorCode_t FILAR_err_get(err_pack err, err_str pid, err_str code);
int out_fifo_pop(u_int rol, u_int *pciaddr, u_int *fragsize, u_int *fragstat, u_int *scw, u_int *ecw);
int in_fifo_push(u_int rol, u_int data);


#ifdef __cplusplus
}
#endif

#endif
