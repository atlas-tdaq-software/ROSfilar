/************************************************************************/
/*									*/
/* File: filar_ioctl.h							*/
/*									*/
/* This header file contains definitions for the ioctl function calls	*/
/* linking the FILAR library to the driver				*/
/*									*/
/* 25. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2002 - The software with that certain something *********/

#ifndef _FILAR_IOCTL_H 
#define _FILAR_IOCTL_H

#include <linux/types.h>

/*************/
/*ioctl codes*/
/*************/
#define FILAR_MAGIC 'x'

#define OPEN       _IOW(FILAR_MAGIC, 66, FILAR_open_t)
#define CLOSE      _IOW(FILAR_MAGIC, 67, FILAR_open_t)
#define CONFIG     _IOW(FILAR_MAGIC, 68, FILAR_config_t)
#define RESET      _IOW(FILAR_MAGIC, 69, int)
#define INFO       _IOR(FILAR_MAGIC, 70, FILAR_info_t)
#define FIFOIN     _IOW(FILAR_MAGIC, 71, int)
#define FLUSH      _IO(FILAR_MAGIC, 72)
#define LINKRESET  _IOW(FILAR_MAGIC, 73, int)
#define LINKSTATUS _IOR(FILAR_MAGIC, 74, int)

/********/
/*Macros*/
/********/
#ifdef __powerpc__
  #define BSWAP(x) bswap(x)
  #define SYNC __asm__("eieio")
#endif

#ifdef __i386__
  #define BSWAP(x) (x)
  #define SYNC
#endif

#endif

