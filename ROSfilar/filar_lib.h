/************************************************************************/
/*									*/
/* File: filar_lib.h							*/
/*									*/
/* This is the private header file for FILAR library			*/
/*									*/
/* 25. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2002 - The software with that certain something *********/

#ifndef _FILAR_LIB_H 
#define _FILAR_LIB_H

#include <linux/types.h>
#include "filar_common.h"

/***************/
/*error strings*/
/***************/
#define FILAR_SUCCESS_STR        "Function successfully executed"
#define FILAR_NOTKNOWN_STR       "Input parameter has illegal value"
#define FILAR_EFAULT_STR         "EFAULT error from driver"
#define FILAR_EIO_STR            "EIO error from driver"
#define FILAR_ENOSYS_STR         "ENOSYS error from driver"
#define FILAR_ILLREV_STR         "FILAR has too old revision"
#define FILAR_IOREMAP_STR        "Error from ioremap system call"
#define FILAR_REQIRQ_STR         "Error from request_irq system call"
#define FILAR_FILE_STR           "Failed to open / close device file"
#define FILAR_NOTOPEN_STR        "The library has not yet been opened"
#define FILAR_NOCARD_STR         "No FILAR card found"
#define FILAR_NO_CODE_STR        "Unknown error"
#define FILAR_KMALLOC_STR        "Error from kmalloc in the driver"
#define FILAR_FIFOFULL_STR       "Cannot write to full FIFO"
#define FILAR_MMAP_STR           "Error from mmap()"
#define FILAR_MUNMAP_STR         "Error from munmap()"
#define FILAR_STUCK_STR          "A link did not come up again after a reset"
#define FILAR_NOHW_STR           "card or channel does not exist"
#define FILAR_EREQ_STR           "Failed to write REQ block"
#define FILAR_EACK_STR           "Failed to receive ACK block"
#define FILAR_USED_STR           "The driver is already being used by another process"
#define FILAR_ALIGN_STR          "PCI address passed to the filar is not 64 bit alignd"

/***************************************/
/*Definitions to be used in the library*/
/*do not modify                        */
/***************************************/
#define DEVICE                  "/dev/filar"


/********/
/*Macros*/
/********/
#define ISOPEN {if(!is_open) return(FILAR_NOTOPEN);}
#define PE(x) {printf("Error from filar library: %s\n",x);     break;}

#ifdef __cplusplus
extern "C" {
#endif
/******************************/
/* Internal service functions */
/******************************/
#ifdef __cplusplus
}
#endif


#endif
