/************************************************************************/
/*									*/
/*  This is the FILAR library 	   				        */
/*  Its supports both the four channel FILAR and the single channel 	*/
/*  FILAR based on the S32PCI64 hardware				*/
/*									*/
/*   8. Jul. 02  MAJO  created						*/
/*									*/
/*******C 2002 - The software with that certain something****************/

#include <iostream>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "ROSfilar/filar.h"
#include "ROSfilar/filar_lib.h"
#include "ROSfilar/filar_ioctl.h"


//Globals
static int fd, is_open = 0;
static u_int outfifor[MAXROLS], infifow[MAXROLS];
static volatile u_int *outfifo, *outfifon, *infifo, *infifon;
static uintptr_t fifobasevirt;
static FILAR_open_t fifos;  
static FILAR_info_t hwstat;  


/********************************/
FILAR_ErrorCode_t FILAR_Open(void)
/********************************/
{  
  u_int loop, ret;
  
  //we need to open the driver only once
  if (is_open)
  {
    is_open++;             //keep track of multiple open calls
    return(RCC_ERROR_RETURN(0, FILAR_SUCCESS));
  }

  //open the error package
  ret = rcc_error_init(P_ID_FILAR, FILAR_err_get);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Open: Failed to open error package");
    return(RCC_ERROR_RETURN(0, FILAR_ERROR_FAIL)); 
  }
  DEBUG_TEXT(DFDB_ROSFILAR, 15 ,"FILAR_Open: error package opened"); 

  //Initialize some arrays
  for(loop = 0; loop < MAXROLS; loop++)
  {
    outfifor[loop] = 0;
    infifow[loop] = 0;
  }

  DEBUG_TEXT(DFDB_ROSFILAR, 15 ,"FILAR_Open: Opening /dev/filar");
  if ((fd = open("/dev/filar", O_RDWR)) < 0)
  {
    perror("open");
    return(RCC_ERROR_RETURN(0, FILAR_FILE)); 
  }
  DEBUG_TEXT(DFDB_ROSFILAR, 15 ,"FILAR_Open: /dev/filar is open");
  
  ret = ioctl(fd, OPEN, &fifos);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Open: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: S/W FIFOS start at 0x" << HEX(fifos.physbase));
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: S/W FIFOS size   = 0x" << HEX(fifos.size));
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: Size of OUT FIFO = 0x" << HEX(fifos.outsize));
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: Size of IN FIFO  = 0x" << HEX(fifos.insize));
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: Size of pointers = 0x" << HEX(fifos.ptrsize));

  // Map the S/W FIFOS into the user virtual address space
  fifobasevirt = (uintptr_t)mmap(0, fifos.size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, (long)fifos.physbase);
  if ((long)fifobasevirt == 0 || (long)fifobasevirt == -1)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Open: Error from mmap, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, FILAR_MMAP)); 
  }
  
  // Define pointers to the relevant FIFO resources
  // This code has to be kept in sync with the respective section in the driver
  outfifo  = (u_int *)fifobasevirt;
  infifo   = (u_int *)(fifobasevirt + fifos.outsize);
  outfifon = (u_int *)(fifobasevirt + fifos.outsize + fifos.insize);
  infifon  = (u_int *)(fifobasevirt + fifos.outsize + fifos.insize + fifos.ptrsize);
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: outfifo  is at 0x" << HEX((uintptr_t)outfifo));
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: infifo   is at 0x" << HEX((uintptr_t)infifo));
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: outfifon is at 0x" << HEX((uintptr_t)outfifon));
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Open: infifon  is at 0x" << HEX((uintptr_t)infifon));

  is_open = 1;
    
  ret = FILAR_Info(&hwstat);  
  if (ret)
    return(RCC_ERROR_RETURN(0, ret)); 
  
  return(FILAR_SUCCESS);
}


/*********************************/
FILAR_ErrorCode_t FILAR_Close(void)
/*********************************/
{
  int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
  
  if (is_open > 1)
    is_open--;
  else
  {  
    DEBUG_TEXT(DFDB_ROSFILAR, 15 ,"FILAR_Close: calling munmap");    
    ret = munmap((void *)fifobasevirt, fifos.size);
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Close: Error from munmap, errno = 0x" << HEX(errno));
      return(RCC_ERROR_RETURN(0, FILAR_MUNMAP)); 
    }
    
    ret = ioctl(fd, CLOSE, &fifos);
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Close: Error from ioctl, errno = 0x" << HEX(errno));
      return(RCC_ERROR_RETURN(0, errno));
    }
  
    close(fd);
    is_open = 0;
  }
  
  return(FILAR_SUCCESS);
}


/**********************************************/
FILAR_ErrorCode_t FILAR_Info(FILAR_info_t *info)
/**********************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
  
  ret = ioctl(fd, INFO, info);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Info: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(FILAR_SUCCESS);
}


/**************************************************/
FILAR_ErrorCode_t FILAR_Init(FILAR_config_t *config)
/**************************************************/
{
  u_int ret, rol;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
  
  //We have to initialize all channels at once since there are some parameters in the FILAR_config_t
  //structure (e.g. page size) which are card specific. 
  for(rol = 0; rol < MAXROLS; rol++)
  {    
    DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Init: config->enable[" << rol << "] = " << config->enable[rol]);
    DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_Init: hwstat.channels[" << rol << "] = " << hwstat.channels[rol]);

    if ((config->enable[rol] == 1) && (hwstat.channels[rol] == 0))
    {
      DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Init: channel " << rol << " does not exist");
      return(RCC_ERROR_RETURN(0, FILAR_NOHW));
    }
  }
    
  ret = ioctl(fd, CONFIG, config);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Init: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(FILAR_SUCCESS);
}


/******************************************/
FILAR_ErrorCode_t FILAR_Reset(u_int channel)
/******************************************/
{
  u_int rol, loop, ret, card;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
  
  if (hwstat.channels[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Reset: channeld " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, FILAR_NOHW));
  }
  
  // NOTE: This will always reset the whole card with all its channels
  card = channel >> 2;  //This will only work for 4 channels per card;
  ret = ioctl(fd, RESET, &card);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Reset: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }

  for (loop = 0; loop < MAXCHANNELS; loop++)
  {
    rol = (card << 2) | loop;
    outfifor[rol] = 0;
    infifow[rol] = 0;
    DEBUG_TEXT(DFDB_ROSFILAR, 20, "FILAR_Reset: resetting S/W FIFOs of ROL " << rol);
  }
  
  return(FILAR_SUCCESS);
}


/**********************************************/
FILAR_ErrorCode_t FILAR_LinkReset(u_int channel)
/**********************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
  
  if (hwstat.channels[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_LinkReset: channeld " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, FILAR_NOHW));
  }
  
  ret = ioctl(fd, LINKRESET, &channel);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_LinkReset: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(FILAR_SUCCESS);
}


/**************************************************************/
FILAR_ErrorCode_t FILAR_LinkStatus(u_int channel, u_int *status)
/**************************************************************/
{
  u_int ret, data;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
  
  if (hwstat.channels[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_LinkReset: channeld " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, FILAR_NOHW));
  }
  
  data = channel;
  ret = ioctl(fd, LINKSTATUS, &data);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_LinkReset: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  *status = data;
  return(FILAR_SUCCESS);
}



/*************************************************/
FILAR_ErrorCode_t FILAR_PagesIn(FILAR_in_t *fifoin)
/*************************************************/
{
  u_int loop, ret, ok;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));

  if (hwstat.channels[fifoin->channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_PagesIn: channel " << fifoin->channel << " does not exist");
    return(RCC_ERROR_RETURN(0, FILAR_NOHW));
  }
           
  ok = 1;               
  DEBUG_TEXT(DFDB_ROSFILAR, 15 ,"FILAR_PagesIn: filling IN FIFO");
  for (loop = 0; loop < fifoin->nvalid; loop++)
  {
    if (fifoin->pciaddr[loop] & 0x3)
    {
      DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_PagesIn: address alignment error");
      return(RCC_ERROR_RETURN(0, FILAR_ALIGN));
    }
    
    ret = in_fifo_push(fifoin->channel, fifoin->pciaddr[loop]);
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_PagesIn: error from in_fifo_push (FIFO full)");
      ok = 0;
      break;   //FIFO is full. Stop filling
    }
  }
  
  //Inform the driver about the new entries. This is needed to get the FILAR driver going.
  //It may not be required later on but the overhead is small.
  ret = ioctl(fd, FIFOIN, &fifoin->channel);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_PagesIn: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  if (ok)
    return(FILAR_SUCCESS);
  else
    return(RCC_ERROR_RETURN(0, FILAR_FIFOFULL));
}


/*********************************************************/
FILAR_ErrorCode_t FILAR_InFree(u_int channel, u_int *nfree)
/*********************************************************/
{  
  int omax, imax;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));

  if (hwstat.channels[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_InFree: channel " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, FILAR_NOHW));
  }
  
  //We have to make sure that the OUT FIFO can never overflow. Therefore the user must not
  //write more pages than would fit into the OUT FIFO. We have to keep in mind that the H/W
  //FIFO does also contain some pages that will eventually end up in the OUT FIFO.  
  
  imax = MAXINFIFO - infifon[channel];
  omax = MAXOUTFIFO - outfifon[channel] - MAXHWFIFO - infifon[channel];
  
  if (imax < omax)
    *nfree = imax;
  else
    *nfree = omax;
  
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_InFree: imax = " << imax << " omax = " << omax << " *nfree = " << *nfree);
  return(FILAR_SUCCESS);
}


/*******************************************************************/
FILAR_ErrorCode_t FILAR_PagesOut(u_int channel, FILAR_out_t *fifoout)
/*******************************************************************/
{
  u_int loop, ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
 
  if (hwstat.channels[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_PagesOut: ERROR: ROL " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, FILAR_NOHW));
  }
 
  loop = 0;
  while(1)  // Until FIFO empty
  {
    ret = out_fifo_pop(channel, &fifoout->pciaddr[loop], &fifoout->fragsize[loop], &fifoout->fragstat[loop], &fifoout->scw[loop], &fifoout->ecw[loop]);
    if (ret)
    {
      fifoout->pciaddr[loop] = 0;
      fifoout->fragsize[loop] = 0;
      fifoout->fragstat[loop] = 0;
      fifoout->scw[loop] = 0;
      fifoout->ecw[loop] = 0;
      break;
    }  
    
    if (fifoout->fragstat[loop])
    {
      DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_PagesOut: Fragstat = " << fifoout->fragstat[loop] << " received from channel " << channel);
    }
    
    DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_PagesOut: read from OUT FIFO: loop = " << loop << " pciaddr = 0x" << HEX(fifoout->pciaddr[loop]) << " fragsize = 0x" << HEX(fifoout->fragsize[loop]) << " fragstat = 0x" << HEX(fifoout->fragstat[loop]));
    loop++;
  }
  fifoout->nvalid = loop;
  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"FILAR_PagesOut: " << fifoout->nvalid << " entries returned");
  
  return(FILAR_SUCCESS);
}


/*********************************/
FILAR_ErrorCode_t FILAR_Flush(void)
/*********************************/
{
  u_int ret, dummy;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
  
  ret = ioctl(fd, FLUSH, &dummy);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Flush: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(FILAR_SUCCESS);
}


/********************************/
FILAR_ErrorCode_t FILAR_Test(void)
/********************************/
{
  u_int ret, dummy;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, FILAR_NOTOPEN));
  
  ret = ioctl(fd, 99, &dummy);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"FILAR_Test: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(FILAR_SUCCESS);
}


/*****************************************************/
//FILAR_ErrorCode_t FILAR_EMCreate(FILAR_in_t *fifoout)
/*****************************************************/
//{
// This function receives the data returned by FILAR_PagesOut and "creates"
// the events. It looks at the "fargsize" and "fragstat" fields of the
// FILAR_out_t structure and writes appropriate information to the ROB / ROD
// headres of the event. It is not yet clear if trhis function should be part of 
// this library, a S/W ROBIN process or the FragmentManager
//}


/*****************************************************************/
unsigned int FILAR_err_get(err_pack err, err_str pid, err_str code)
/*****************************************************************/
{ 
  strcpy(pid, P_ID_FILAR_STR);

  switch (RCC_ERROR_MINOR(err))
  {  
    case FILAR_SUCCESS:       strcpy(code, FILAR_SUCCESS_STR);     break;
    case FILAR_FILE:          strcpy(code, FILAR_FILE_STR);        break;
    case FILAR_NOTOPEN:       strcpy(code, FILAR_NOTOPEN_STR);     break;
    case FILAR_EFAULT:        strcpy(code, FILAR_EFAULT_STR);      break;
    case FILAR_EIO:           strcpy(code, FILAR_EIO_STR);         break;
    case FILAR_ENOSYS:        strcpy(code, FILAR_ENOSYS_STR);      break;
    case FILAR_ILLREV:        strcpy(code, FILAR_ILLREV_STR);      break;
    case FILAR_IOREMAP:       strcpy(code, FILAR_IOREMAP_STR);     break;
    case FILAR_REQIRQ:        strcpy(code, FILAR_REQIRQ_STR);      break;
    case FILAR_NOCARD:        strcpy(code, FILAR_NOCARD_STR);      break;
    case FILAR_KMALLOC:       strcpy(code, FILAR_KMALLOC_STR);     break;
    case FILAR_FIFOFULL:      strcpy(code, FILAR_FIFOFULL_STR);    break;
    case FILAR_MMAP:          strcpy(code, FILAR_MMAP_STR);        break;
    case FILAR_MUNMAP:        strcpy(code, FILAR_MUNMAP_STR);      break;
    case FILAR_NOHW:          strcpy(code, FILAR_NOHW_STR);        break;
    case FILAR_STUCK:         strcpy(code, FILAR_STUCK_STR);       break;
    case FILAR_USED:          strcpy(code, FILAR_USED_STR);        break;
    case FILAR_ALIGN:         strcpy(code, FILAR_ALIGN_STR);       break;
    default:                  strcpy(code, FILAR_NO_CODE_STR);     return(RCC_ERROR_RETURN(0, FILAR_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, FILAR_SUCCESS));
}


/*************************************/
int in_fifo_push(u_int rol, u_int data)
/*************************************/
{
  if (infifon[rol] == MAXINFIFO)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 5 ,"in_fifo_push: The IN FIFO is full");
    return(-1);
  }

  DEBUG_TEXT(DFDB_ROSFILAR, 20 ,"in_fifo_push: index = " << IN_INDEX(rol, infifow[rol]));  

  infifo[IN_INDEX(rol, infifow[rol])] = data;
  infifow[rol]++;
  if (infifow[rol] == MAXINFIFO)
    infifow[rol] = 0;
  
  infifon[rol]++;
  return(0);
}


/***************************************************************************************************/
int out_fifo_pop(u_int rol, u_int *pciaddr, u_int *fragsize, u_int *fragstat, u_int *scw, u_int *ecw)
/***************************************************************************************************/
{
  int oo;
    
  DEBUG_TEXT(DFDB_ROSFILAR, 20, "out_fifo_pop: rol = " << rol << "  outfifon[rol] = " << outfifon[rol] << " at " << &outfifon[rol]);
  if (outfifon[rol] == 0)
  {
    DEBUG_TEXT(DFDB_ROSFILAR, 20, "out_fifo_pop: The OUT FIFO is empty");
    *pciaddr = 0;
    *fragsize = 0;
    *fragstat = 0;
    *scw = 0;
    *ecw = 0;
    return(-1);
  }
  
  oo = OUT_INDEX(rol, outfifor[rol]);
  *pciaddr = outfifo[oo];
  *fragsize = outfifo[oo + 1];
  *fragstat = outfifo[oo + 2];
  *scw = outfifo[oo + 3];
  *ecw = outfifo[oo + 4];

  //printf("out_fifo_pop: at oo index %d we have *pciaddr = 0x%08x, *fragsize = 0x%08x, *fragstat = 0x%08x\n", oo, *pciaddr, *fragsize, *fragstat);


  outfifor[rol]++;
  if (outfifor[rol] == MAXOUTFIFO)
    outfifor[rol] = 0;
  
  outfifon[rol]--;
  return(0);
}
