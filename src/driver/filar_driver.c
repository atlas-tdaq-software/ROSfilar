/************************************************************************/
/*									*/
/* File: filar_driver.c							*/
/*									*/
/* driver for the FILAR S-Link interface				*/
/*									*/
/* 25. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2003 - The software with that certain something *********/

/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.4 onwards			*/
/************************************************************************/

/************************************************************************/
/*Open issues:								*/
/*- The ISR can have errors from the FIFOS (unlikely). It is not clear 	*/
/*  how such errors would be reported. For now they are just logged	*/
/*- The start and end control word patters passed via the INIT ioctl    */
/*  are not yet used in the ISR for consistency checking.		*/
/*- The ioctl fuctions copy large arrays into the driver and back to	*/
/*  the user application. This is so because the amount of data in the	*/
/*  arrays is not known. If possible one should only copy the valid 	*/
/*  data. I don't know yet how to do this				*/ 
/************************************************************************/

// Module Versioning a la Rubini p.316

#include <linux/config.h>

#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
  #define MODVERSIONS
#endif

#if defined(MODVERSIONS)
  #include <linux/modversions.h>
#endif

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include "ROSfilar/filar_driver.h"

#ifdef MODULE
  MODULE_PARM (debug, "i");
  MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
  MODULE_PARM (errorlog, "i");
  MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");
  MODULE_DESCRIPTION("S-Link FILAR");
  MODULE_AUTHOR("Markus Joos, CERN/EP");
  #ifdef MODULE_LICENSE
    MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
  #endif
#endif


/*********/
/*Globals*/
/*********/
static int debug = 0, errorlog = 1;
static int filar_major = 0; // use dynamic allocation
static volatile T_filar_regs *filar[MAXCARDS];
static u_int fuse = 1, scw, ecw, filar_irq_line[MAXCARDS];
static u_int maxcard, channels[MAXCARDS][MAXCHANNELS], served[MAXCARDS][MAXCHANNELS], pci_memaddr[MAXCARDS];
static u_char pci_revision[MAXCARDS];
static u_int irqlist[MAXIRQ], cmask[MAXCARDS], hwfifo[MAXROLS][MAXHWFIFO];
static u_int opid, hwfifow[MAXROLS], hwfifor[MAXROLS], hwfifon[MAXROLS];
static u_int *outfifo, *outfifon;
static u_int *infifo, *infifon;
static u_int infifor[MAXROLS], outfifow[MAXROLS];
static u_int ccw, order, swfifobasephys, swfifobase, outfifodatasize, infifodatasize, fifoptrsize, tsize = 0;
static struct proc_dir_entry *filar_file;
struct filar_proc_data_t filar_proc_data;
static u_int pcibuff; //MJ: for debugging
static u_int ackdata[MAXROLS][MAXOUTFIFO][4], acknum[MAXROLS]; 
static char *proc_read_text;


/************/
/*Prototypes*/
/************/
static void filar_irq_handler (int irq, void *dev_id, struct pt_regs *regs);  
static void filar_vclose(struct vm_area_struct *vma);


static struct file_operations fops = 
{
  owner:   THIS_MODULE,
  mmap:    filar_mmap,
  ioctl:   filar_ioctl,
  open:    filar_open,    
  release: filar_release
};

// memory handler functions
static struct vm_operations_struct filar_vm_ops = 
{
  close:  filar_vclose,   // mmap-close
};

/*****************************/
/* Standard driver functions */
/*****************************/


/**********************************************************/
static int filar_open(struct inode *ino, struct file *filep)
/**********************************************************/
{
  //MOD_INC_USE_COUNT;
  return(0);
}


/*************************************************************/
static int filar_release(struct inode *ino, struct file *filep)
/*************************************************************/
{
  //MOD_DEC_USE_COUNT;
  opid = 0;
  return(0);
}

/*************************************************************************************************/
static int filar_ioctl(struct inode *inode, struct file *file, u_int cmd, unsigned long arg)
/*************************************************************************************************/
{
  switch (cmd)
  {    
    case OPEN:
    {
      FILAR_open_t params;
      
      kdebug(("filar(ioctl,OPEN): called from process %d\n", current->pid))
      if (!opid)
        opid = current->pid;
      else
      {
	kerror(("filar(ioctl, OPEN): The FILAR is already used by process %d\n", opid));
    	return(-FILAR_USED);
      }
	
      params.size = tsize;
      params.physbase = swfifobasephys;
      params.outsize = outfifodatasize;
      params.insize = infifodatasize;
      params.ptrsize = fifoptrsize;
      kdebug(("filar(ioctl, OPEN):  tsize           = 0x%08x\n", tsize));
      kdebug(("filar(ioctl, OPEN):  swfifobasephys  = 0x%08x\n", swfifobasephys));
      kdebug(("filar(ioctl, OPEN):  outfifodatasize = 0x%08x\n", outfifodatasize));
      kdebug(("filar(ioctl, OPEN):  infifodatasize  = 0x%08x\n", infifodatasize));
      kdebug(("filar(ioctl, OPEN):  fifoptrsize     = 0x%08x\n", fifoptrsize));
            
      if (copy_to_user((void *)arg, &params, sizeof(FILAR_open_t)) != 0)
      {
	kerror(("filar(ioctl, OPEN): error from copy_to_user\n"));
	return(-FILAR_EFAULT);
      }      
      return(0);
      break;
    }
    
    case CLOSE:
    {
      kdebug(("filar(ioctl,CLOSE): called from process %d\n", current->pid))
      opid = 0;
  
      return(0);
      break;
    }
    
    case CONFIG:
    { 
      FILAR_config_t params;
      u_int card, channel, data, rol;
      
      if (copy_from_user(&params, (void *)arg, sizeof(FILAR_config_t)) !=0 )
      {
	kerror(("filar(ioctl,CONFIG): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
        
      scw = params.scw;
      ecw = params.ecw;
      ccw = params.ccw;

      kdebug(("filar(ioctl,CONFIG): params.bswap = %d\n", params.bswap));
      kdebug(("filar(ioctl,CONFIG): params.wswap = %d\n", params.wswap));
      kdebug(("filar(ioctl,CONFIG): params.psize = %d\n", params.psize));
      kdebug(("filar(ioctl,CONFIG): params.ccw = %d\n", params.ccw));

      for(card = 0; card < maxcard; card++)
      {
        data = 0;
        for(channel = 0; channel < MAXCHANNELS; channel++)
        { 
          served[card][channel] = 0;
          rol = (card << 2) + channel;  //works only for 4 channels per card
          kdebug(("filar(ioctl,CONFIG): params.enable[%d] = %d\n", rol, params.enable[rol]));
          if (!params.enable[rol])
          {
            data += (1 << (6 * channel + 9));
            channels[card][channel] = 0;
            kdebug(("filar(ioctl,CONFIG): channels[%d][%d] = %d\n", card, channel, channels[card][channel]));
          }
          else
          {
            channels[card][channel] = 1; 
            kdebug(("filar(ioctl,CONFIG): channels[%d][%d] = %d\n", card, channel, channels[card][channel]));
          }

          kdebug(("filar(ioctl,CONFIG): data = 0x%08x\n", data));
        }
        data += (params.bswap << 1) + (params.wswap << 2) + (params.psize << 3);
        kdebug(("filar(ioctl,CONFIG): Writing 0x%08x to the OCR\n", data));
        filar[card]->ocr = data;
        cmask[card] = data;        //remember which channels were enabled
        
        if (params.interrupt)
          filar[card]->imask |= 0x2;  //Enable the ACK FIFO almost full interrupt 
      }
      break;
    }
    
    case RESET:
    {
      u_int channel, data, card, rol;
      
      if (copy_from_user(&card, (void *)arg, sizeof(int)) !=0 )
      {
	kerror(("filar(ioctl,RESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }

      for(channel = 0; channel < MAXCHANNELS; channel++)
        served[card][channel] = 0;
      
      if (card <= maxcard)
      {
        kdebug(("filar(ioctl,RESET): Resetting card %d\n", card));
        data = filar[card]->ocr;
        data |= 0x1;
        filar[card]->ocr = data;
        // Delay for at least one us
        udelay(2);
        data &= 0xfffffffe;
        filar[card]->ocr = data;

        //reset the FIFOs
        for(channel = 0; channel < MAXCHANNELS; channel++)
        {
          rol = (card << 2) + channel;
          infifor[rol] = 0;
          infifon[rol] = 0;
          outfifow[rol] = 0;
          outfifon[rol] = 0;
          hwfifow[rol] = 0;
          hwfifor[rol] = 0;
          hwfifon[rol] = 0;
        }
      }
      else
      {
       	kerror(("filar(ioctl,RESET): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      
      return(0);
      break;
    }
    
    case LINKRESET:
    {   
      u_int waited, rol, data, card, channel;
 
      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0 )
      {
	kerror(("filar(ioctl,RESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      
      card = rol >> 2;      
      if (card <= maxcard)
      {
        channel = rol & 0x3;
        kdebug(("filar(ioctl,LINKRESET): Resetting card %d, channel %d\n", card, channel));
        served[card][channel] = 0;

        // Set the URESET bit
        data = 0x1 << (8 + channel * 6);
        filar[card]->ocr |= data;
        
        // Delay for at least 30 ms
        mdelay(30);

        // Now wait for LDOWN to come up again
        data = 0x1 << (16 + channel * 4);
        waited = 0;
        while(filar[card]->osr & data)
        {
          waited++;
          if (waited > 10000)
          {
            kerror(("filar(ioctl,LINKRESET): channel %d of card %d does not come up again\n", channel, card));
            // Reset the URESET bit
            data = ~(0x1 << (8 + channel * 6));
            filar[card]->ocr &= data;
	    return(-FILAR_STUCK);
          }          
        }

        // Reset the URESET bit
        data = ~(0x1 << (8 + channel * 6));
        filar[card]->ocr &= data;
      }
      else
      {
       	kerror(("filar(ioctl,LINKRESET): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      
      kdebug(("filar(ioctl,LINKRESET): Done\n"));
      return(0);
      break;
    }
    
    case LINKSTATUS:
    {   
      u_int isup, data, card, rol, channel;
      
      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0 )
      {
	kerror(("filar(ioctl, RESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      
      card = rol >> 2;
      if (card <= maxcard)
      {
        channel = rol & 0x3;
        data = 0x1 << (16 + channel * 4);
        if (filar[card]->osr & data)
          isup = 0;
        else
          isup = 1;
          
        if (copy_to_user((void *)arg, &isup, sizeof(int)) != 0)
        {
	  kerror(("filar(ioctl, LINKSTATUS): error from copy_to_user\n"));
	  return(-FILAR_EFAULT);
        } 
      }
      else
      {
       	kerror(("filar(ioctl, LINKSTATUS): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      
      return(0);
      break;
    }

    case INFO:
    {
      FILAR_info_t info;
      u_int rol, card, data;
     
      //Initialise the array to 0
      for(rol = 0; rol < MAXROLS; rol++)
      {
        info.channels[rol] = 0;
        info.enabled[rol] = 0;
      }
      info.nchannels = 0;
      
      for(card = 0; card < maxcard; card++)
      {
        data = filar[card]->osr;
        kdebug(("filar(ioctl, INFO): 0x%08x read from OSR\n", data));
        if (data & 0x00080000)  info.channels[(card << 2)] = 0; 
        else {info.channels[(card << 2)] = 1; info.nchannels++;}
        
        if (data & 0x00800000)  info.channels[(card << 2) + 1] = 0; 
        else {info.channels[(card << 2) + 1] = 1; info.nchannels++;}
        
        if (data & 0x08000000)  info.channels[(card << 2) + 2] = 0; 
        else {info.channels[(card << 2) + 2] = 1; info.nchannels++;}
        
        if (data & 0x80000000)  info.channels[(card << 2) + 3] = 0; 
        else {info.channels[(card << 2) + 3] = 1; info.nchannels++;}
                
        data = filar[card]->ocr;
        kdebug(("filar(ioctl, INFO): 0x%08x read from OCR\n", data));
        
        if (data & 0x00000200) info.enabled[(card << 2)] = 0;     else info.enabled[(card << 2)] = 1;
        if (data & 0x00008000) info.enabled[(card << 2) + 1] = 0; else info.enabled[(card << 2) + 1] = 1;        
        if (data & 0x00200000) info.enabled[(card << 2) + 2] = 0; else info.enabled[(card << 2) + 2] = 1;        
        if (data & 0x08000000) info.enabled[(card << 2) + 3] = 0; else info.enabled[(card << 2) + 3] = 1;       
      }  

      if (copy_to_user((void *)arg, &info, sizeof(FILAR_info_t)) != 0)
      {
	kerror(("filar(ioctl, INFO): error from copy_to_user\n"));
	return(-FILAR_EFAULT);
      }      
      return(0);
      break;
    }
    
    case FIFOIN:
    { 
      u_int card, channel, rol, loop, nfree, navail, min, data;
      unsigned long flags;

      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("filar(ioctl,FIFOIN): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      //marker(0x70000000 | rol);
      card = rol >> 2;
      channel = rol & 0x3; 
      kdebug(("filar(ioctl,FIFOIN): channel = %d\n", channel));
      kdebug(("filar(ioctl,FIFOIN): card = %d\n", card));

      //We have to make sure that this code does not get interrupted. Otherwhise there will be
      //overflows in the IN FIFO which lead to incorrect PCI addresses being written to the FILAR
      save_flags(flags);
      cli();   //MJ: for 2.6 see P274 & p 275

      //Is there space in the FILAR
      hw_fifo_level(rol, &nfree);
      kdebug(("filar(ioctl,FIFOIN): There are %d slots available in the HW FIFO of ROL %d\n", nfree, rol));

      //How many entries are in the IN FIFO
      navail = infifon[rol]; 
      kdebug(("filar(ioctl,FIFOIN): There are %d entries in the IN FIFO\n", navail));

      if (nfree < navail)
        min = nfree;
      else
        min = navail;
              
      kdebug(("filar(ioctl,FIFOIN): min = %d\n", min));
                 
      for(loop = 0; loop < min; loop++)
      {
        // We have checked how many entries are free / available in the FIFOS
        // Therefore we can waive the return values
        in_fifo_pop(rol, &data);
        hw_fifo_push(rol, data);

        // I don't check if there is space as this should be guaranteed by
        // the coupling between the FILAR FIFO and the H/W FIFO which has the same depth
             if (channel == 0) filar[card]->req1 = data;
        else if (channel == 1) filar[card]->req2 = data;
        else if (channel == 2) filar[card]->req3 = data;
        else                   filar[card]->req4 = data; 
	
	pcibuff = data; //MJ: for debugging

	     
        kdebug(("filar(ioctl,FIFOIN): PCI addr. 0x%08x written to card %d, channel %d\n", data, card, channel));
      }

      restore_flags(flags);
      //marker(0x7f000000 | rol);
      return(0);
      break;
    }
    
    case FLUSH:
    { 
      flush_cards();
      break;
    }
    
    case 99:
    {
      volatile u_int data, *ptr;
      kdebug(("filar(ioctl,99): function called\n"));

      ptr = &filar[0]->osr;

      data = *ptr;
      data = *ptr;
      data = *ptr;
      data = *ptr;
      data = *ptr;
      data = *ptr;
      
      *ptr=data;
      *ptr=data;
      *ptr=data;
      *ptr=data;
      *ptr=data;
      *ptr=data;

      data = *ptr;
      *ptr=data;
      data = *ptr;
      *ptr=data;
      data = *ptr;
      *ptr=data;
      
      return(0);
      break;
    }
  }   
  return(0);
}


/****************************************************************************************************/
static int filar_write_procmem(struct file *file, const char *buffer, unsigned long count, void *data)
/****************************************************************************************************/
{
  int res, len;
  u_int card;
  struct filar_proc_data_t *fb_data = (struct filar_proc_data_t *)data;

  kdebug(("filar(filar_write_procmem): filar_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len))
  {
    kerror(("filar(filar_write_procmem): error from copy_from_user\n"));
    return(-FILAR_EFAULT);
  }

  kdebug(("filar(filar_write_procmem): len = %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("filar(filar_write_procmem): text passed = %s\n", fb_data->value));

  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("filar(filar_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("filar(filar_write_procmem): debugging disabled\n"));
    debug = 0;
  }

  if (!strcmp(fb_data->value, "disable"))
  {
    for(card = 0; card < maxcard; card++)
      filar[card]->ocr |= 0x08208200;
    kdebug(("filar(filar_write_procmem): All channels disabled\n"));
  }
  
  if (!strcmp(fb_data->value, "elog"))
  {
    kdebug(("filar(filar_write_procmem): Error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(fb_data->value, "noelog"))
  {
    kdebug(("filar(filar_write_procmem): Error logging disabled\n"))
    errorlog = 0;
  }
  
  if (!strcmp(fb_data->value, "reset"))
  {
    reset_cards(1);
    kdebug(("filar(filar_write_procmem): All cards reset\n"));
  }
    
  if (!strcmp(fb_data->value, "flush"))
  {
    flush_cards();
    kdebug(("filar(filar_write_procmem): All cards flushed\n"));
  }
  
  if (!strcmp(fb_data->value, "dec"))
  {
    MOD_DEC_USE_COUNT;
    kdebug(("filar(filar_write_procmem): Use count decremented\n")); 
  }
  
  if (!strcmp(fb_data->value, "inc"))
  {
    MOD_INC_USE_COUNT;
    kdebug(("filar(filar_write_procmem): Use count incremented\n")); 
  }
  
  return len;
}


/***************************************************************************************************/
static int filar_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/***************************************************************************************************/
{
  u_int rol, ret, card, ocr[MAXROLS], osr[MAXROLS], osr2[MAXROLS], imask[MAXROLS], fifostat[MAXROLS], value, value2, channel;
  int loop, nchars = 0;
  static int len = 0;

  kdebug(("filar(filar_read_procmem): Called with buf    = 0x%08x\n", (u_int)buf));
  kdebug(("filar(filar_read_procmem): Called with *start = 0x%08x\n", (u_int)*start));
  kdebug(("filar(filar_read_procmem): Called with offset = %d\n", (u_int)offset));
  kdebug(("filar(filar_read_procmem): Called with count  = %d\n", count));

  if (offset == 0)
  {
    kdebug(("exp(exp_read_procmem): Creating text....\n"));
    len = 0;
    len += sprintf(proc_read_text + len, "\n\n\nFILAR driver (single cycle  protocol) for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);

  if  (opid)
    len += sprintf(proc_read_text + len, "The drivers is currently used by PID %d\n", opid);
    len += sprintf(proc_read_text + len, "=========================================================================\n");
    len += sprintf(proc_read_text + len, "Card|IRQ line| ACK IRQ|revision|PCI MEM addr.|         page size|wswap|bswap|temperature\n");
    len += sprintf(proc_read_text + len, "----|--------|--------|--------|-------------|------------------|-----|-----|-----------\n");

    //Read the registers
    for(card = 0; card < maxcard; card++)
    {
      ocr[card]   = filar[card]->ocr;
      osr[card]   = filar[card]->osr;
      osr2[card]  = filar[card]->osr;
      imask[card] = filar[card]->imask;
      fifostat[card] = filar[card]->fifostat;
    }

    for(card = 0; card < maxcard; card++)
    {
      len += sprintf(proc_read_text + len, "   %d|", card);
      len += sprintf(proc_read_text + len, "     %3d|", filar_irq_line[card]);
      len += sprintf(proc_read_text + len, "%s|", (imask[card] & 0x2)?" enabled":"disabled"); 
      len += sprintf(proc_read_text + len, "    0x%02x|", pci_revision[card]); 
      len += sprintf(proc_read_text + len, "   0x%08x|", pci_memaddr[card]);
      value = (ocr[card] >> 3) & 0x7;
      if (value == 0) len += sprintf(proc_read_text + len, "         256 bytes|");
      if (value == 1) len += sprintf(proc_read_text + len, "           1 Kbyte|");
      if (value == 2) len += sprintf(proc_read_text + len, "          2 Kbytes|");
      if (value == 3) len += sprintf(proc_read_text + len, "          4 Kbytes|");
      if (value == 4) len += sprintf(proc_read_text + len, "         16 Kbytes|");
      if (value == 5) len += sprintf(proc_read_text + len, "         64 Kbytes|");
      if (value == 6) len += sprintf(proc_read_text + len, "        256 Kbytes|");
      if (value == 7) len += sprintf(proc_read_text + len, "4 MBytes - 8 Bytes|");
      len += sprintf(proc_read_text + len, "  %s|", (ocr[card] & 0x4)?"yes":" no");
      len += sprintf(proc_read_text + len, "  %s|", (ocr[card] & 0x2)?"yes":" no");
      len += sprintf(proc_read_text + len, " %3d deg. C\n", (osr[card] >>8) & 0xff);
    }

    for(card = 0; card < maxcard; card++)
    {
      len += sprintf(proc_read_text + len, "\nCard %d:\n", card);
      len += sprintf(proc_read_text + len, "=======\n");
      len += sprintf(proc_read_text + len, "       |       |       |          |free      |     |        |       |          |free      |\n");
      len += sprintf(proc_read_text + len, "       |       |       |entries in|entries in|     |        |       |entries in|entries in|fragments\n");
      len += sprintf(proc_read_text + len, "Channel|present|enabled|ACK FIFO  |REQ FIFO  |LDOWN|Overflow|   XOFF|OUT FIFO  |IN FIFO   |served\n");
      len += sprintf(proc_read_text + len, "-------|-------|-------|----------|----------|-----|--------|-------|----------|----------|---------\n");

      for(channel  = 0; channel < MAXCHANNELS; channel++)
      {
	len += sprintf(proc_read_text + len, "      %d|", channel);

	value = osr[card] & (1 << (19 + channel * 4));
	len += sprintf(proc_read_text + len, "    %s|", value?" no":"yes");

	value = ocr[card] & (1 << (9 + channel * 6));
	len += sprintf(proc_read_text + len, "    %s|", value?" no":"yes");

	value = (fifostat[card] >> (channel * 8)) & 0xf;
	if (value < 15)
          len += sprintf(proc_read_text + len, "        %2d|", value);
	else
          len += sprintf(proc_read_text + len, "       >14|");

	value = (fifostat[card] >> ( 4 + channel * 8)) & 0xf;
	if (value < 15)
          len += sprintf(proc_read_text + len, "        %2d|", value);
	else
          len += sprintf(proc_read_text + len, "       >14|");

	value = osr[card] & (1 << (16 + channel * 4));
	len += sprintf(proc_read_text + len, "  %s|", value?"yes":" no"); 

	value = osr[card] & (1 << (17 + channel * 4));
	len += sprintf(proc_read_text + len, "     %s|", value?"yes":" no");

	value = osr[card] & (1 << (18 + channel * 4));
	value2 = osr2[card] & (1 << (18 + channel * 4));
	if (!value && !value2)
          len += sprintf(proc_read_text + len, " is off|");
	else if (value && value2)
          len += sprintf(proc_read_text + len, "  is on|");
	else if (value && !value2)
          len += sprintf(proc_read_text + len, " was on|");
	else 
          len += sprintf(proc_read_text + len, "went on|");

	rol = (card << 2) + channel;
	ret = out_fifo_level(rol, &value);
	len += sprintf(proc_read_text + len, "      %4d|", MAXOUTFIFO - value);

	ret = in_fifo_level(rol, &value);
	len += sprintf(proc_read_text + len, "      %4d|", value);

	len += sprintf(proc_read_text + len, " %8d\n", served[card][channel]);     
      }
    }

    len += sprintf(proc_read_text + len, " \n");
    len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/filar', executed as root,\n");
    len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
    len += sprintf(proc_read_text + len, "debug   -> enable debugging\n");
    len += sprintf(proc_read_text + len, "nodebug -> disable debugging\n");
    len += sprintf(proc_read_text + len, "elog    -> Log errors to /var/log/message\n");
    len += sprintf(proc_read_text + len, "noelog  -> Do not log errors to /var/log/message\n");
    len += sprintf(proc_read_text + len, "disable -> disable all FILAR channels\n");
    len += sprintf(proc_read_text + len, "reset   -> Reset all FILAR channels\n");
    len += sprintf(proc_read_text + len, "flush   -> Flush all FILAR channels\n");
    len += sprintf(proc_read_text + len, "dec     -> Decrement use count\n");
    len += sprintf(proc_read_text + len, "inc     -> Increment use count\n");
  }
  kdebug(("filar(filar_read_procmem): number of characters in text buffer = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("filar(filar_read_procmem): min nchars         = %d\n", nchars));
  
  if (nchars > 0)
  {
    for (loop = 0; loop < nchars; loop++)
      buf[loop + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("filar(filar_read_procmem): returning *start   = 0x%08x\n", (u_int)*start));
  kdebug(("filar(filar_read_procmem): returning nchars   = %d\n", nchars));
  return(nchars);
}

/**************************/
static int init_module(void)
/**************************/
{
  int rol, tfsize, result;
  u_int loop;
  struct page *page_ptr;

  kerror(("FILAR (SC) driver version 1.0\n")); 

  SET_MODULE_OWNER(&fops);

  //Clear the list of used interupts 
  for(loop = 0; loop < MAXIRQ; loop++)
    irqlist[loop] = 0;

  result = init_filars();
  if (result)
  {
    kerror(("filar(init_module): init_filar() failed\n"));
    return(result);
  }

  result = register_chrdev(filar_major, "filar", &fops); 
  if (result < 1)
  {
    kerror(("filar(init_module): registering FILAR driver failed.\n"));
    return(-FILAR_EIO);
  }
  filar_major = result;

  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kdebug(("filar(init_module): error from kmalloc\n"));
    return(-EFAULT);
  }

  filar_file = create_proc_entry("filar", 0644, NULL);
  if (filar_file == NULL)
  {
    kerror(("filar(init_module): error from call to create_proc_entry\n"));
    return (-ENOMEM);
  }

  strcpy(filar_proc_data.name, "filar");
  strcpy(filar_proc_data.value, "filar");
  filar_file->data = &filar_proc_data;
  filar_file->read_proc = filar_read_procmem;
  filar_file->write_proc = filar_write_procmem;
  filar_file->owner = THIS_MODULE;

  // Allocate contiguous memory for the FIFOs and store the base addresses in a global structure
  outfifodatasize = MAXROLS * MAXOUTFIFO * OUTFIFO_ELEMENTS * sizeof(u_int);    //outfifo
  infifodatasize = MAXROLS * MAXINFIFO * INFIFO_ELEMENTS * sizeof(u_int);       //infifo
  fifoptrsize = MAXROLS * sizeof(u_int);                      //outfifo(w/r/n) + infifo(w/r/n)
  kdebug(("filar(init_module): 0x%08x bytes needed for the OUT FIFO\n", outfifodatasize));
  kdebug(("filar(init_module): 0x%08x bytes needed for the IN FIFO\n", infifodatasize));
  kdebug(("filar(init_module): 0x%08x bytes needed for the FIFO pointers\n", fifoptrsize));
  
  tsize = outfifodatasize + infifodatasize + 2 * fifoptrsize;
  kdebug(("filar(init_module): 0x%08x bytes needed for all FIFOs\n", tsize));

  tfsize = (tsize + 4095) & 0xfffff000;  // round up to next 4K boundary
  tfsize = tfsize >> 12;                 // compute number of pages
  kdebug(("filar(init_module): %d pages needed for the S/W FIFOs\n", tfsize));
 
  order = 0;
  while(tfsize)
  {
    order++;
    tfsize = tfsize >> 1;                 // compute order
  } 
  kdebug(("filar(init_module): order = %d\n", order));
  
  swfifobase = __get_free_pages(GFP_ATOMIC, order);
  if (!swfifobase)
  {
    kerror(("rfilar(init_module): error from __get_free_pages\n"));
    return(-FILAR_EFAULT);
  }
  kdebug(("filar(init_module): swfifobase = 0x%08x\n", swfifobase));
   
  // Reserve all pages to make them remapable
  // This code fragment has been taken from the PCI (SHASLink) driver of M. Mueller
  page_ptr = virt_to_page(swfifobase);

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    set_bit(PG_reserved, &page_ptr->flags);

  swfifobasephys = virt_to_bus((void *) swfifobase);
  kdebug(("filar(init_module): swfifobasephys = 0x%08x\n", swfifobasephys));

  // Assign base addresses to the FIFO arrays
  outfifo  = (u_int *)swfifobase;
  infifo   = (u_int *)(swfifobase + outfifodatasize);
  outfifon = (u_int *)(swfifobase + outfifodatasize + infifodatasize);
  infifon  = (u_int *)(swfifobase + outfifodatasize + infifodatasize + fifoptrsize);

  kdebug(("filar(init_module): outfifo  is at 0x%08x\n", (u_int)outfifo));
  kdebug(("filar(init_module): infifo   is at 0x%08x\n", (u_int)infifo));
  kdebug(("filar(init_module): outfifon is at 0x%08x\n", (u_int)outfifon));
  kdebug(("filar(init_module): infifon  is at 0x%08x\n", (u_int)infifon));

  //Initialize the FIFOs
  for(rol = 0; rol < MAXROLS; rol++)
  {
    infifor[rol] = 0;
    infifon[rol] = 0;
    outfifow[rol] = 0;
    outfifon[rol] = 0;
    hwfifow[rol] = 0;
    hwfifor[rol] = 0;
    hwfifon[rol] = 0;
  }

  opid = 0;

  kdebug(("filar(init_module): driver loaded; major device number = %d\n", filar_major));
  return(0);
}


/******************************/
static void cleanup_module(void)
/******************************/
{
  u_int loop;
  struct page *page_ptr;
      
  cleanup_filars();

  // unreserve all pages used for the S/W FIFOs
  page_ptr = virt_to_page(swfifobase);

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    clear_bit(PG_reserved, &page_ptr->flags);

  // free the area 
  free_pages(swfifobase, order);

  if (unregister_chrdev(filar_major, "filar") != 0) 
  {
    kerror(("filar(cleanup_module): cleanup_module failed\n"));
  }
  
  remove_proc_entry("filar", NULL);
  kfree(proc_read_text);

  kdebug(("filar(cleanup_module): driver removed\n"));
}


/*************************************************************************/
static void filar_irq_handler (int irq, void *dev_id, struct pt_regs *regs)   //MJ: for 2.6 see p272 & p273 
/*************************************************************************/
{
  u_int data, card;

  // Note: the FILAR card(s) may have to share an interrupt line with other PCI devices
  // It is therefore important to exit quickly if we are not concerned with an interrupt
  // For now this is done by looking at the FIFOSTAT register. There may be a dedicated
  // interrupt status bit in the future.
  
  //marker(0x40000000 | irq); 

  for(card = 0; card < maxcard; card++)
  {
    if ((filar_irq_line[card] == (u_int)irq) && (filar[card]->osr & 0x2))
    {
      if (fuse)
      {
        //marker(0x42000000 | card);
        read_ack_and_write_out(card);
      }
      else
      {    
        //marker(0x43000000 | card);
        filar[card]->imask = 0;  //Disable all interrupts
        kerror(("filar(filar_irq_handler): CATASTROPHY: Interrupt received from card %d with fuse=0\n", card));
      }
    }
  }
  //marker(0x4f000000 | irq);
}  //MJ: for 2.6 see p279


/**************************/
static int init_filars(void)
/**************************/
{
  struct pci_dev *filar_dev[MAXCARDS];   // "soft" FILAR structures
  u_char pci_bus[MAXCARDS], pci_device_fn[MAXCARDS];
  u_int ok, result, card, loop;
  
  // Find all FILAR modules
  maxcard = 0;
  for(card = 0; card < MAXCARDS; card++)
  {
    filar_dev[card] = 0;
    ok = 1;
    for(loop = 0; loop < (card + 1); loop++)
    {
      kdebug(("filar(init_filars): loop = %d  card = %d  MAXCARDS = %d\n", loop, card, MAXCARDS));
      filar_dev[card] = pci_find_device(PCI_VENDOR_ID_CERN, 0x14, filar_dev[card]);  //Find N-th device
      if (filar_dev[card] == NULL)
      {
        kdebug(("filar(init_filars): FILAR %d not found\n", loop + 1));
        ok = 0;
        break;
      }
      else
      {
        kdebug(("filar(init_filars): FILAR %d found\n", loop + 1));
      }
    }
    if (ok)
      maxcard++;
  }
  
  if (!maxcard)   // No FILARs found
    return(-FILAR_ENOSYS);
    
  kdebug(("filar(init_filars): %d FILARs found\n", maxcard));
  
  // Initialize the available FILAR modules
  for(card = 0; card < maxcard; card++)
  {
    pci_bus[card] = filar_dev[card]->bus->number;
    pci_device_fn[card] = filar_dev[card]->devfn;

    pci_memaddr[card] = filar_dev[card]->resource[0].start;

    // get revision directly from the Filar
    pci_read_config_byte(filar_dev[card], PCI_REVISION_ID, &pci_revision[card]);

    kdebug(("filar(init_filars): Filar found: \n"));
    kdebug(("  bus = %x device = %x revision = %x address = 0x%08x\n", pci_bus[card], pci_device_fn[card], pci_revision[card], pci_memaddr[card]));

    if (pci_revision[card] < MINREV)
    {
      kerror(("filar(init_filars): Illegal Filar Revision\n"));
      return(-FILAR_ILLREV);
    }

    if ((pci_memaddr[card] & PCI_BASE_ADDRESS_SPACE) == PCI_BASE_ADDRESS_SPACE_IO)
    {
      kerror(("filar(init_filars): The device uses IO addresses\n"));
      kerror(("filar(init_filars): But this driver is made for memory mapped access\n"));
      return(-FILAR_EIO);
    }

    filar[card] = (T_filar_regs *)ioremap(pci_memaddr[card] & PCI_BASE_ADDRESS_MEM_MASK, 4 * 1024);

    pci_read_config_dword(filar_dev[card], PCI_INTERRUPT_LINE, &filar_irq_line[card]);
    kdebug(("filar(init_filars): interrupt pin = %d, interrupt line = %d \n", (filar_irq_line[card] & 0x0000ff00) >> 8, filar_irq_line[card] & 0x000000ff));

    filar_irq_line[card] &= 0x000000ff;

    if (irqlist[filar_irq_line[card]] == 0)
    {
      result = request_irq(filar_irq_line[card], filar_irq_handler, SA_SHIRQ, "filar", filar_irq_handler);	// Rubini p.275
      if (result)
      {
        kdebug(("filar(init_filars): request_irq failed on IRQ = %d\n", filar_irq_line[card]));
        return(-FILAR_REQIRQ);
      }
      irqlist[filar_irq_line[card]] = 1;
      kdebug(("filar(init_filars): Interrupt %d registered\n", filar_irq_line[card])); 
    }  
      
    kdebug(("filar(init_filars): request_irq OK\n"));
  }
  
  return(0);
}	


/*****************************/
static int cleanup_filars(void)
/*****************************/
{
  u_int card;

  for(card = 0; card < maxcard; card++)
  {
    if (irqlist[filar_irq_line[card]] == 1)
    {
      free_irq(filar_irq_line[card], filar_irq_handler);
      kdebug(("filar(cleanup_filar): interrupt line %d released\n", filar_irq_line[card]));
      irqlist[filar_irq_line[card]] = 0;
    }

    iounmap((void *)filar[card]);
  }

  return(0);
}


//------------------
// Service functions
//------------------


/****************************************/
static int hw_fifo_push(int rol, int data)
/****************************************/
{
  if (hwfifon[rol] == MAXHWFIFO)
  {
    kerror(("filar(hw_fifo_push): The HW FIFO is full\n"));
    blow_fuse();
    return(-1);
  }
  hwfifo[rol][hwfifow[rol]] = data;
  hwfifow[rol]++;
  if (hwfifow[rol] == MAXHWFIFO)
    hwfifow[rol] = 0;
  
  hwfifon[rol]++;
  return(0);
}


/****************************************/
static int hw_fifo_pop(int rol, int *data)
/****************************************/
{
  if (hwfifon[rol] == 0)
  {
    kerror(("filar(hw_fifo_pop): The HW FIFO is empty\n"));
    blow_fuse();
    return(-1);
  }
  *data = hwfifo[rol][hwfifor[rol]];
  hwfifor[rol]++;
  if (hwfifor[rol] == MAXHWFIFO)
    hwfifor[rol] = 0;
  
  hwfifon[rol]--;
  return(0);
}


/*******************************************/
static int hw_fifo_level(int rol, int *nfree)
/*******************************************/
{
  *nfree = MAXHWFIFO - hwfifon[rol];
  return(0);
}


/****************************************/
static int in_fifo_pop(int rol, int *data)
/****************************************/
{
  if (infifon[rol] == 0)
  {
    kdebug(("filar(in_fifo_pop): The IN FIFO is empty\n"));
    return(-1);
  }
  *data = infifo[IN_INDEX(rol, infifor[rol])];

  infifor[rol]++;
  if (infifor[rol] == MAXINFIFO)
    infifor[rol] = 0;

  infifon[rol]--;

  return(0);
}


/*******************************************/
static int in_fifo_level(int rol, int *nfree)
/*******************************************/
{
  *nfree = MAXINFIFO - infifon[rol];
  return(0);
}


/******************************************************************************************/
static int out_fifo_push(int rol, int pciaddr, int fragsize, int fragstat, int scw, int ecw)
/******************************************************************************************/
{
  int oo;
  
  oo = OUT_INDEX(rol, outfifow[rol]);
  kdebug(("filar(out_fifo_push): oo = %d\n", oo));
  
  if (outfifon[rol] == MAXOUTFIFO)
  {
    kerror(("filar(out_fifo_push): The OUT FIFO is full\n"));
    return(-1);
  }
  
  outfifo[oo]     = pciaddr;
  outfifo[oo + 1] = fragsize;
  outfifo[oo + 2] = fragstat;
  outfifo[oo + 3] = scw;
  outfifo[oo + 4] = ecw;

  outfifow[rol]++;
  if (outfifow[rol] == MAXOUTFIFO)
    outfifow[rol] = 0;
  outfifon[rol]++;
  return(0);
}


/********************************************/
static int out_fifo_level(int rol, int *nfree)
/********************************************/
{
  *nfree = MAXOUTFIFO - outfifon[rol];
  return(0);
}


/******************************************************************/
static int filar_mmap(struct file *file, struct vm_area_struct *vma)
/******************************************************************/
{
  unsigned long offset, size;

  kdebug(("filar(filar_mmap): function called\n"));
  
  offset = vma->vm_pgoff << PAGE_SHIFT;
  
  size = vma->vm_end - vma->vm_start;

  kdebug(("filar(filar_mmap): offset = 0x%08x\n",(u_int)offset));
  kdebug(("filar(filar_mmap): size   = 0x%08x\n",(u_int)size));

  if (offset & ~PAGE_MASK)
  {
    kerror(("filar(filar_mmap): offset not aligned: %ld\n", offset));
    return -ENXIO;
  }

  // we only support shared mappings. "Copy on write" mappings are
  // rejected here. A shared mapping that is writeable must have the
  // shared flag set.
  if ((vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_SHARED))
  {
    kerror(("filar(filar_mmap): writeable mappings must be shared, rejecting\n"));
    return(-EINVAL);
  }

  vma->vm_flags |= VM_RESERVED;
  
  // we do not want to have this area swapped out, lock it
  vma->vm_flags |= VM_LOCKED;

  // we create a mapping between the physical pages and the virtual
  // addresses of the application with remap_page_range.
  // enter pages into mapping of application
  kdebug(("filar(filar_mmap): Parameters of remap_page_range()\n"));
  kdebug(("filar(filar_mmap): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("filar(filar_mmap): Physical address = 0x%08x\n",(u_int)offset));
  kdebug(("filar(filar_mmap): Size             = 0x%08x\n",(u_int)size));
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,20)
  if (remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot))
#else
  if (remap_page_range(vma->vm_start, offset, size, vma->vm_page_prot))
#endif
  {
    kerror(("filar(filar_mmap): remap page range failed\n"));
    return -ENXIO;
  }
  
  vma->vm_ops = &filar_vm_ops;  
  //MOD_INC_USE_COUNT;
  
  kdebug(("filar(filar_mmap): function done\n"));
  return(0);
}


/**************************************************/
static void filar_vclose(struct vm_area_struct *vma)
/**************************************************/
{  
  kdebug(("filar(filar_vclose): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("filar(filar_vclose): mmap released\n"));
  //MOD_DEC_USE_COUNT;
}


/*****************************/
static void disable_input(void)
/*****************************/
{
  u_int card, data;

  //marker(0x20000000);
  for(card = 0; card < maxcard; card++)
  {
    data = filar[card]->ocr;
    data |= 0x08208200;
    filar[card]->ocr = data;   //disable all channels
  }
  //marker(0x21000000);
}


/****************************/
static void enable_input(void)
/****************************/
{
  u_int card;

  //marker(0x22000000);
  for(card = 0; card < maxcard; card++)
    filar[card]->ocr = cmask[card];  //reset cannels to initial state
  //marker(0x23000000);
}


/******************************************/
static void read_ack_and_write_out(int card)
/******************************************/
{
  u_int data2, fragstat, nfree, pciaddr, ret, dindex, rol, loop, nacks, channel;
  volatile u_int data, *ackptr, *reqptr;

  //---> Part 1: read / write FILAR registers
  //marker(0x90000000 | card);
  disable_input();
  for(channel = 0; channel < MAXCHANNELS; channel++)
  {
    kdebug(("filar(read_ack_and_write_out): channels[%d][%d] = %d\n", card, channel, channels[card][channel]));
    if (channels[card][channel])
    {
      //marker(0x91000000 | channel);
      dindex = 0;
      rol = (card << 2) + channel;     
      reqptr = (u_int *) (&filar[card]->req1 + 4 * channel);
      ackptr = (u_int *) reqptr + 1;
      out_fifo_level(rol, &nfree); 

      while(1)
      {   
        nacks = (filar[card]->fifostat >> (channel * 8)) & 0xf;
        kdebug(("filar(read_ack_and_write_out): card=%d  channel=%d  rol=%d  nacks=%d\n", card, channel, rol, nacks));
        //marker(0x92000000 | nacks);
        if (!nacks)
          break;

        for(loop = 0; loop < nacks; loop++)
        {  
	  ret = hw_fifo_pop(rol, &pciaddr);
          if (ret)
          {
            kerror(("filar(read_ack_and_write_out): error from hw_fifo_pop\n"));
	    blow_fuse();
            return;
          }
          kdebug(("filar(read_ack_and_write_out): Read PCI address 0x%08x from HW FIFO\n", pciaddr));

	  data = *ackptr;
          kdebug(("filar(read_ack_and_write_out): Read 0x%08x from ACK FIFO\n", data));
          ackdata[rol][dindex][0] = data;
          if ((data & 0x40000000) || ccw)
            ackdata[rol][dindex][1] = filar[card]->scw;
          if ((data & 0x10000000) || ccw)
            ackdata[rol][dindex][2] = filar[card]->ecw;
	    
          ackdata[rol][dindex][3] = pciaddr;
          dindex++;
	  nfree--;
	  if (!nfree)
          {
            kerror(("filar(read_ack_and_write_out): nfree=0 , dindex=%d-> blowing fuse\n", dindex));
            blow_fuse();
            return;
          }
        }
      }
      
      acknum[rol] = dindex;             
      kdebug(("filar(read_ack_and_write_out): Received %d fragments from card=%d, channel=%d\n", dindex, card, channel));

      for(loop = 0; loop < dindex; loop++)
      {
        ret = in_fifo_pop(rol, &pciaddr);
        if (!ret)
        {
	  ret = hw_fifo_push(rol, pciaddr);
          if (ret)
          {
            kerror(("filar(read_ack_and_write_out): error from hw_fifo_push\n"));
            blow_fuse();
            return;
          }
          *reqptr = pciaddr;
          kdebug(("filar(read_ack_and_write_out): Wrote PCI address 0x%08x to REQ FIFO\n", pciaddr));
	}  
      }
    }
  }
  enable_input();
  
  //---> Part 2: process data
  for(channel = 0; channel < MAXCHANNELS; channel++)
  {
    if (channels[card][channel])
    {
      rol = (card << 2) + channel;
      for(loop = 0; loop < acknum[rol]; loop++)
      {        
        fragstat = 0;
        data = ackdata[rol][loop][0];
        kdebug(("filar(read_ack_and_write_out): Analyzing card=%d  channel=%d  fragment=%d  ack=0x%08x\n", card, channel, loop, data));
        if (data & 0x80000000)
        {
          fragstat |= NO_SCW;
          kerror(("filar(read_ack_and_write_out): NO_SCW"));
        }
        else 
        {
          if (data & 0x40000000)
          {
            fragstat |= BAD_SCW;
            kerror(("filar(read_ack_and_write_out): BAD_SCW"));

            data2 = ackdata[rol][loop][1];
            kerror(("filar(read_ack_and_write_out): Start Control Word              = 0x%08x\n", data2));

            if (data2 & 0x1)
            {
              kerror(("filar(read_ack_and_write_out): Data transmission error in SCW\n"));
              fragstat |= SCW_DTE;
            }

            if (data2 & 0x2)
            {
              kerror(("filar(read_ack_and_write_out): Control word transmission error in SCW\n"));
              fragstat |= SCW_CTE;
            }
          }
        }

        if (data & 0x20000000)
        {
          fragstat |= NO_ECW;
          kerror(("filar(read_ack_and_write_out): NO_ECW"));
        }
        else
        {
          if (data & 0x10000000)
          {
            fragstat |= BAD_ECW;
            kerror(("filar(read_ack_and_write_out): BAD_ECW"));

            data2 = ackdata[rol][loop][2];  
            kerror(("filar(read_ack_and_write_out): End Control Word                = 0x%08x\n", data2));

            if (data2 & 0x1)
            {
              kerror(("filar(read_ack_and_write_out): Data transmission error in ECW\n"));
              fragstat |= ECW_DTE;
            }

            if (data2 & 0x2)
            {
              kerror(("filar(read_ack_and_write_out): Control word transmission error in ECW\n"));
              fragstat |= ECW_CTE;
            } 
          }
        }

        if (ccw)
         ret = out_fifo_push(rol, ackdata[rol][loop][3], data & 0x000fffff, fragstat, ackdata[rol][loop][1], ackdata[rol][loop][2]);
	else
         ret = out_fifo_push(rol, ackdata[rol][loop][3], data & 0x000fffff, fragstat, 0 ,0);
    
        if (ret)
        {
          kerror(("filar(read_ack_and_write_out): error from out_fifo_push\n"));
          blow_fuse();
	  return;
        }        
      }
      served[card][channel] += acknum[rol];
    }
  } 
  //marker(0x1f000000);
}


/*************************/
static void blow_fuse(void)
/*************************/
{   // If this fuse blows one has to re-configure the FILAR (to re-enable the interrupt)
  u_int data, card, channel, rol, nacks, nfree;

  marker(0xe1000000);
  fuse = 0;
  kerror(("filar(blow_fuse): There was an internal error in the driver\n"));
  kerror(("filar(blow_fuse): All interrupt have been disabled\n"));
  kerror(("filar(blow_fuse): You have to reload the driver\n"));

  for(card = 0; card < MAXCARDS; card++)
  {
    for(channel = 0; channel < MAXCHANNELS; channel++)
    {
      if (channels[card][channel])
      {
        rol = (card << 2) + channel;
        nacks = (filar[card]->fifostat >> (channel * 8)) & 0xf;
        out_fifo_level(rol, &nfree); 
        kerror(("filar(blow_fuse): rol=%d  nacks=%d  nfree=%d\n", rol, nacks, nfree));
      }
    }
  } 

  for(card = 0; card < maxcard; card++)
  {
    filar[card]->imask = 0;  //Disable all interrupts
    
    data = filar[card]->ocr;
    data |= 0x1;
    filar[card]->ocr = data;
    udelay(2);  // Delay for at least one us
    data &= 0xfffffffe;
    filar[card]->ocr = data;
  }
}

/**************************/
static void marker(int data)
/**************************/
{
  u_int card;

  for(card = 0; card < maxcard; card++)
    filar[card]->scw = data;
}


/*******************************/
static void reset_cards(int mode)
/*******************************/
{
  u_int card, rol, channel, data;

  kdebug(("filar(reset_cards): maxcard = %d   mode = %d\n", maxcard, mode));
  for(card = 0; card < maxcard; card++)
  {
    data = filar[card]->ocr;
    data |= 0x1;
    filar[card]->ocr = data;
    // Delay for at least one us
    udelay(10);
    data &= 0xfffffffe;
    filar[card]->ocr = data;
    //reset the FIFOs
    if (mode)
    {
      for(channel = 0; channel < MAXCHANNELS; channel++)
      {
        served[card][channel] = 0;
        rol = (card << 2) + channel;
        infifor[rol] = 0;
        infifon[rol] = 0;
        outfifow[rol] = 0;
        outfifon[rol] = 0;
        hwfifow[rol] = 0;
        hwfifor[rol] = 0;
        hwfifon[rol] = 0;
      }
    }
  }
  kdebug(("filar(reset_cards): Done\n"));
}


/***************************/
static void flush_cards(void)
/***************************/
{
  unsigned long flags;
  u_int card, channel;

  //We have to make sure that this code does not get interrupted.
  //Otherwhise there can be a race condition.
  save_flags(flags);
  cli();
  //marker(0x30000000);
  for(card = 0; card < maxcard; card++)
  {
    kdebug(("filar(flush_cards): Calling read_ack_and_write_out for card %d\n", card));
    read_ack_and_write_out(card);
  }
  //marker(0x31000000);

  for(channel = 0; channel < MAXCHANNELS; channel++)
  {
    kdebug(("filar(flush_cards): There are %d events in the OUT FIFO of channel %d\n", outfifon[channel], channel));
  }

  restore_flags(flags);
}

