/****************************************************************/
/*                                                              */
/*  file: pcinblast.c                                           */
/*                                                              */
/*  This program allows to benchmark 5V and 3.3V PCI slot.      */
/*  It runs on a S32PCI64 card with the PCIBLASTER firmware     */
/*  as well as on the AMCC based 32-bit PCIBLASTER		*/
/*                                                              */
/*  Author: Markus Joos, CERN-EP                                */
/*                                                              */
/*   2.05.01  MAJO  created                                     */
/*                                                              */
/****************C 2001 - A nickel program worth a dime**********/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "cmem_rcc/cmem_rcc.h"
#include "io_rcc/io_rcc.h"
#include "ROSGetInput/get_input.h"

#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

#define DMA_READ    0
#define DMA_WRITE   1
#define READ        2
#define READ_MULTI  0
#define READ_LINE   1
#define WRITE       0
#define WRITE_INV   1

  
//constants
#define MAXBUF            2
#define TREF              15.015
#define BUFSIZE           (32*1024)        //bytes
#define PREFILL           0xfeedbabe
#define S32_MCSR          0x3c             //AMCC S5933
#define S32_MWAR          0x24             //AMCC S5933
#define S32_MRAR          0x2c             //AMCC S5933
#define S32_MWTC          0x28             //AMCC S5933
#define S32_MRTC          0x30             //AMCC S5933
#define AMCC_READ_ENABLE  0x00004000       //MCSR read xfer enable
#define AMCC_WRITE_ENABLE 0x00000400       //MCSR write xfer enable


//types
typedef struct
{
  u_int ocr;       //0x000
  u_int osr;       //0x004
  u_int timer;     //0x008
  u_int res1[61];  //0x00c-0x0fc
  u_int raddr;     //0x100
  u_int rsize;     //0x104
  u_int rcount;    //0x108
  u_int res2[61];  //0x10c-0x1fc
  u_int waddr;     //0x200
  u_int wsize;     //0x208
  u_int wcount;    //0x20c
} T_pciblast_regs;

typedef struct
{
  u_int dir;
  u_int size;
  u_int count;
  u_int paddr;
  u_int ctype;
} tparams;  


//globals
static u_int cont;
static u_int handle_32, handle_64;
static u_long sreg, sreg32, uaddr[MAXBUF], paddr[MAXBUF];
static volatile T_pciblast_regs *pciblast;
static volatile int *s_mcsr, *s_mwar, *s_mwtc, *s_mrar, *s_mrtc;
static int bhandle[MAXBUF];
static int have32, have64;
static struct sigaction sa;
 
//some prototypes 
int cardreset(void);
  
/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
  cont = 0;
  debug(("SigQuitHandler: ctrl+// received\n"));
}


/***********************/
int s32pci64_map(int num)
/***********************/
{
  u_int eret, memadd;
  
  have64 = 0;
  
  eret = IO_Open();
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }

  eret = IO_PCIDeviceLink(0x10dc, 0x0013, num, &handle_64);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }  

  eret = IO_PCIConfigReadUInt(handle_64, 0x10, &memadd);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  eret = IO_PCIMemMap(memadd, 0x400, &sreg);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 
  
  pciblast=(T_pciblast_regs *)sreg;
  have64 = 1;
  return(0);
}


/**********************/
int s32pci64_unmap(void)
/**********************/
{
  u_int eret;   
  
  eret = IO_PCIMemUnmap(sreg, 0x1000);
  if(eret)
    rcc_error_print(stdout,eret);

  eret = IO_PCIDeviceUnlink(handle_64);
  if(eret)
    rcc_error_print(stdout,eret);
    
  eret = IO_Close();
  if(eret)
    rcc_error_print(stdout,eret);
    
  return(0);
}


/***********************/
int s32pci32_map(int num)
/***********************/
{
  u_int eret, memadd;

  have32 = 0;
  
  eret = IO_Open();
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }

  eret = IO_PCIDeviceLink(0x10dc, 0x0011, num, &handle_32);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }  

  eret = IO_PCIConfigReadUInt(handle_32, 0x10, &memadd);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 


  eret = IO_PCIMemMap(memadd, 0x1000, &sreg32);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  s_mcsr=(int *)((int)sreg32 + S32_MCSR);
  s_mwar=(int *)((int)sreg32 + S32_MWAR);
  s_mwtc=(int *)((int)sreg32 + S32_MWTC);
  s_mrar=(int *)((int)sreg32 + S32_MRAR);
  s_mrtc=(int *)((int)sreg32 + S32_MRTC);
  have32 = 1;
  return(0);
}


/**********************/
int s32pci32_unmap(void)
/**********************/
{
  u_int eret;   
    
  eret = IO_PCIMemUnmap(sreg32, 0x1000);
  if(eret)
    rcc_error_print(stdout,eret);

  eret = IO_PCIDeviceUnlink(handle_32);
  if(eret)
    rcc_error_print(stdout,eret);
    
  eret = IO_Close();
  if(eret)
    rcc_error_print(stdout,eret);
    
  return(0);
}


/***************/
int dumpmem(void)
/***************/
{
  u_int data, value;
  float atime;
  
  printf("\n\n==============================================================\n");
  data = pciblast->ocr;
  printf("\nOperation Control register (0x%08x)\n", data);

  printf("Write cycle type:     %s\n", (data & 0x80)?"Memory write and invalidate":"Memory write");
  value = (data >> 5) & 0x3;
  printf("Read cycle type:      ");
  if (value == 0) printf("Memory read multiple\n");
  if (value == 1) printf("Memory read line\n");
  if (value == 2) printf("Memory read\n");
  if (value == 3) printf("<out of range>\n");

  printf("Board reset active:   %s\n", (data & 0x10)?"Yes":"No");
  printf("Write channel active: %s\n", (data & 0x04)?"Yes":"No");
  printf("Read channel active:  %s\n", (data & 0x01)?"Yes":"No");
  
  data = pciblast->osr;
  printf("\nOperation Status register (0x%08x)\n", data);
  printf("Write channel busy:   %s\n", (data & 0x04)?"Yes":"No");
  printf("Read channel busy:    %s\n", (data & 0x01)?"Yes":"No");
  
  data = pciblast->timer;
  atime = data * TREF / 1000.0;  // time in us
  printf("\nTimer value:          0x%08x (%f us)\n", data, atime);
  
  data = pciblast->raddr;
  printf("\nRead address:         0x%08x\n", data);
  data = pciblast->rsize;
  printf("Read size:            %d words\n", data & 0xffffff);
  data = pciblast->rsize;
  printf("Read loop count:      %d\n", data & 0xffffff);

  data = pciblast->waddr;
  printf("\nWrite address:        0x%08x\n", data);
  data = pciblast->wsize;
  printf("Write size:           %d words\n", data & 0xffffff);
  data = pciblast->wsize;
  printf("Write loop count:     %d\n", data & 0xffffff);

  printf("==============================================================\n");
  return(0);
}


/****************/
int uio_init(void)
/****************/
{
  u_int loop,eret;

  eret = CMEM_Open();
  if (eret)
  {
    printf("Sorry. Failed to open the cmem_rcc library\n");
    rcc_error_print(stdout, eret);
    exit(6);
  }

  for(loop = 0; loop < MAXBUF; loop++)
  {
    eret = CMEM_SegmentAllocate(BUFSIZE, "slink", &bhandle[loop]);
    if (eret)
    {
      printf("Sorry. Failed to allocate buffer #%d\n", loop + 1);
      rcc_error_print(stdout, eret);
      exit(7);
    }

    eret = CMEM_SegmentVirtualAddress(bhandle[loop], &uaddr[loop]);
    if (eret)
    {
      printf("Sorry. Failed to get virtual address for buffer #%d\n", loop + 1);
      rcc_error_print(stdout, eret);
      exit(8);
    }

    eret = CMEM_SegmentPhysicalAddress(bhandle[loop], &paddr[loop]);
    if (eret)
    {
      printf("Sorry. Failed to get physical address for buffer #%d\n", loop + 1);
      rcc_error_print(stdout, eret);
      exit(9);
    }
  }
  return(0);
}


/****************/
int uio_exit(void)
/****************/
{
  u_int loop, eret;

  for(loop = 0; loop < MAXBUF; loop++)
  {
    eret = CMEM_SegmentFree(bhandle[loop]);
    if (eret)
    {
      printf("Warning: Failed to free buffer #%d\n", loop + 1);
      rcc_error_print(stdout, eret);
    }
  }

  eret = CMEM_Close();
  if (eret)
  {
    printf("Warning: Failed to close the CMEM_RCC library\n");
    rcc_error_print(stdout, eret);
  }
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n\n==============================================================\n");
  printf("Call Markus Joos, 72364, 160663 if you need help\n");
  printf("==============================================================\n");
  return(0);
}


/*****************/
int readbench(void)
/*****************/
{
  static u_int ctype = 2, mode = 1, size = 0x100, count = 100;
  u_int data;
  float atime, mbps;

  printf("\n\n==============================================================\n");

  printf("Do you want to use polling (1=yes  0=no): ");
  mode = getdecd(mode);
 
  printf("Select the cycle type:\n");
  printf(" 0 = Memory read multiple\n");
  printf(" 1 = Memory read line\n");
  printf(" 2 = Memory read\n");
  printf("Your choice: ");
  ctype = getdecd(ctype);
 
  if (mode == 1)
  {
    printf("Enter the size in bytes (max. 0x%08x): ",BUFSIZE);
    size = size << 2;   //convert from words to bytes
    size = gethexd(size);
    size = size >> 2;   //convert from bytes to words
    printf("Enter the loop count: ");
    count = getdecd(count);
  }
  else
  {
    count = 1;
    size = 0x1000;
  }
  printf("PCI address = 0x%016lx\n",paddr[1]);
  cardreset();
  pciblast->rsize = size;
  pciblast->rcount = count;
  pciblast->raddr =  paddr[1];
  pciblast->ocr = (ctype << 5) | 0x1;

  if (mode == 1)
  {
    while(pciblast->osr & 0x1);     // poll for EOT
    data = pciblast->timer;
    atime = data * TREF / 1000.0;  // time in us
    printf("\nTimer value:          0x%08x (%f us)\n", data, atime);
    printf("Total transfer size   0x%x bytes\n", size * count * 4);
    atime /=1000000.0;              // time in s
    mbps = size * count * 4 / atime / 1024.0 / 1024.0;
    printf("Performance (read)    %f MB/s\n",mbps);
  }

  printf("==============================================================\n");
  return(0);
}


/******************/
int writebench(void)
/******************/
{
  static u_int ctype = 0, mode = 1, size = 0x100, count = 100;
  u_int data;
  float atime, mbps;

  printf("\n\n==============================================================\n");

  printf("Do you want to use polling (1=yes  0=no): ");
  mode = getdecd(mode);

  printf("Select the cycle type:\n");
  printf(" 0 = Memory write\n");
  printf(" 1 = Memory write and invalidate\n");
  printf("Your choice: ");
  ctype = getdecd(ctype);

  if (mode == 1)
  {
    printf("Enter the size in bytes (max. 0x%08x): ",BUFSIZE);
    size = size << 2;   //convert from words to bytes
    size = gethexd(size);
    size = size >> 2;   //convert from bytes to words
    printf("Enter the loop count: ");
    count = getdecd(count);
  }
  else
  {
    count = 1;
    size = 0x1000;
  }

  printf("PCI address = 0x%016lx\n",paddr[0]);
  cardreset();
  pciblast->wsize = size;
  pciblast->wcount = count;
  pciblast->waddr =  paddr[0];
  pciblast->ocr = (ctype << 7) | 0x4;

  if (mode == 1)
  {
    while(pciblast->osr & 0x4);     // poll for EOT
    data = pciblast->timer;
    atime = data * TREF / 1000.0;   // time in us
    printf("\nTimer value:          0x%08x (%f us)\n", data, atime);
    printf("Total transfer size   0x%x bytes\n", size * count * 4);
    atime /=1000000.0;              // time in s
    mbps = size * count * 4 / atime / 1024.0 / 1024.0;
    printf("Performance (write)   %f MB/s\n",mbps);
  }
  printf("==============================================================\n");
  return(0);
}


/********************************/
int dodma(tparams tp, float *mbps)
/********************************/
{
  float atime;

  //printf("size = %d  count = %d dir = %d  ctype = %d\n",tp.size, tp.count, tp.dir, tp.ctype);
  cardreset();
  if (tp.dir == DMA_WRITE)
  {
    pciblast->wsize  = tp.size;
    pciblast->wcount = tp.count;
    pciblast->waddr  = tp.paddr;
    pciblast->ocr = (tp.ctype << 7) | 0x4;
    while(pciblast->osr & 0x4);     // poll for EOT
  }
  else
  {
    pciblast->rsize  = tp.size;
    pciblast->rcount = tp.count;
    pciblast->raddr  = tp.paddr;
    pciblast->ocr = (tp.ctype << 5) | 0x1;
    while(pciblast->osr & 0x1);     // poll for EOT
  }

  atime = pciblast->timer * TREF / 1000000000.0;   // time in s
  *mbps = tp.size * tp.count * 4 / atime / 1024.0 / 1024.0;
  //printf("counter = %d  atime = %f  mbps = %f\n",pciblast->timer, atime, *mbps); 

  return(0);
}


/*****************/
int cardreset(void)
/*****************/
{
  pciblast->ocr = 0x10;
  sleep(1);
  pciblast->ocr = 0x0;
  return(0);
}


/***************/
int loop64w(void)
/***************/
{
  static u_int ctype = 0, size = 0x100, count = 100000;
  float atime, mbps;
  tstamp ts2, tso;
  
  printf("\n\n==============================================================\n");

  printf("Select the cycle type:\n");
  printf(" 0 = Memory write\n");
  printf(" 1 = Memory write and invalidate\n");
  printf("Your choice: ");
  ctype = getdecd(ctype);

  printf("Enter the size in bytes (max. 0x%08x): ",BUFSIZE);
  size = size << 2;   //convert from words to bytes
  size = gethexd(size);
  size = size >> 2;   //convert from bytes to words

  printf("PCI address = 0x%016lx\n",paddr[0]);

  cont = 1;
  printf("Press crtl+\\ to terminate\n");
  cardreset();
  while(cont)
  {
    pciblast->ocr = 0x10;
    ts_delay(1);
    pciblast->ocr = 0x0;
    pciblast->wsize = size;
    pciblast->wcount = count;
    pciblast->waddr = paddr[0];
    pciblast->ocr = (ctype << 7) | 0x4;

    while(pciblast->osr & 0x4);     // poll for EOT
    ts_clock(&ts2);
    atime = ts_duration(tso, ts2);  // time in s
    tso = ts2;
    mbps = size * count * 4 / atime / 1024.0 / 1024.0;
    printf("Performance (write)   %f MB/s\n",mbps);
  }
  printf("==============================================================\n");
  return(0);
}


/***************/
int loop64r(void)
/***************/
{
  static u_int ctype = 0, size = 0x100, count = 100000;
  float atime, mbps;
  tstamp tso, ts2;
  
  printf("\n\n==============================================================\n");

  printf("Select the cycle type:\n");
  printf(" 0 = Memory read multiple\n");
  printf(" 1 = Memory read line\n");
  printf(" 2 = Memory read\n");
  printf("Your choice: ");
  ctype = getdecd(ctype);
  
  printf("Enter the size in bytes (max. 0x%08x): ",BUFSIZE);
  size = size << 2;   //convert from words to bytes
  size = gethexd(size);
  size = size >> 2;   //convert from bytes to words

  printf("PCI address = 0x%016lx\n",paddr[1]);

  cont = 1;
  printf("Press crtl+\\ to terminate\n");
  while(cont)
  {  
    pciblast->ocr = 0x10;
    ts_delay(1);
    pciblast->ocr = 0x0;
    pciblast->rsize = size;
    pciblast->rcount = count;
    pciblast->raddr = paddr[1];
    pciblast->ocr = (ctype << 5) | 0x1;
  
    while(pciblast->osr & 0x1);     // poll for EOT
    ts_clock(&ts2);
    atime = ts_duration(tso, ts2);  // time in s
    tso = ts2;
    mbps = size * count * 4 / atime / 1024.0 / 1024.0;
    printf("Performance (write)   %f MB/s\n",mbps);
  }
  printf("==============================================================\n");
  return(0);
}


/**************/
int abench(void)
/**************/
{
  float atime, mbps[15], mbpsr, mbpsw; 
  tparams tp;
  volatile int data;
  int loop;
  tstamp ts1, ts2;

  tp.paddr = paddr[0];
  tp.dir = DMA_WRITE; tp.size  = 100;   tp.count = 1;   tp.ctype = WRITE;      dodma(tp, &mbps[0]);
  tp.dir = DMA_WRITE; tp.size  = 10000; tp.count = 1;   tp.ctype = WRITE;      dodma(tp, &mbps[1]);
  tp.dir = DMA_WRITE; tp.size  = 100;   tp.count = 100; tp.ctype = WRITE;      dodma(tp, &mbps[2]);
  tp.dir = DMA_WRITE; tp.size  = 100;   tp.count = 1;   tp.ctype = WRITE_INV;  dodma(tp, &mbps[3]);
  tp.dir = DMA_WRITE; tp.size  = 10000; tp.count = 1;   tp.ctype = WRITE_INV;  dodma(tp, &mbps[4]);
  tp.dir = DMA_WRITE; tp.size  = 100;   tp.count = 100; tp.ctype = WRITE_INV;  dodma(tp, &mbps[5]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 1;   tp.ctype = READ;       dodma(tp, &mbps[6]);
  tp.dir = DMA_READ;  tp.size  = 10000; tp.count = 1;   tp.ctype = READ;       dodma(tp, &mbps[7]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 100; tp.ctype = READ;       dodma(tp, &mbps[8]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 1;   tp.ctype = READ_MULTI; dodma(tp, &mbps[9]);
  tp.dir = DMA_READ;  tp.size  = 10000; tp.count = 1;   tp.ctype = READ_MULTI; dodma(tp, &mbps[10]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 100; tp.ctype = READ_MULTI; dodma(tp, &mbps[11]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 1;   tp.ctype = READ_LINE;  dodma(tp, &mbps[12]);
  tp.dir = DMA_READ;  tp.size  = 10000; tp.count = 1;   tp.ctype = READ_LINE;  dodma(tp, &mbps[13]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 100; tp.ctype = READ_LINE;  dodma(tp, &mbps[14]);
  
  printf("\n\n=========================================================================\n");
  printf("All results in MB/s\n");
  printf("PCI bursts          |  1 * 100 bytes |  1 * 10000 bytes | 100 * 100 bytes |\n");
  printf("====================|================|==================|=================|\n");
  printf("Write               |      %9.1f |        %9.1f |       %9.1f |\n", mbps[0], mbps[1], mbps[2]);
  printf("Write & invalidate  |      %9.1f |        %9.1f |       %9.1f |\n", mbps[3], mbps[4], mbps[5]);
  printf("Read                |      %9.1f |        %9.1f |       %9.1f |\n", mbps[6], mbps[7], mbps[8]);
  printf("Read multiple       |      %9.1f |        %9.1f |       %9.1f |\n", mbps[9], mbps[10], mbps[11]);
  printf("Read line           |      %9.1f |        %9.1f |       %9.1f |\n", mbps[12], mbps[13], mbps[14]);
  printf("==========================================================================|\n");
  
  ts_clock(&ts1);
  for(loop = 1000; loop--; loop)
    data = pciblast->wsize;      
  ts_clock(&ts2);
  atime = ts_duration(ts1, ts2);
  mbpsr = 4000 / atime / 1024.0 / 1024.0;

  ts_clock(&ts1);
  for(loop = 1000; loop--; loop)
    pciblast->wsize = 0;
  ts_clock(&ts2);
  atime = ts_duration(ts1, ts2);
  mbpsw = 4000 / atime / 1024.0 / 1024.0;
  
  printf("\n");
  printf("PCI single cycles   |        32 bit |\n");
  printf("====================|===============|\n");
  printf("Write               |     %9.1f |\n", mbpsw);
  printf("Read                |     %9.1f |\n", mbpsr);
  return(0);
}


/**********************************/
int dodma32(tparams tp, float *mbps)
/**********************************/
{
  float atime;
  int loop;
  tstamp ts1, ts2;
  
  if (tp.dir == DMA_READ)
  { 
    *s_mcsr = AMCC_READ_ENABLE;
    if (tp.ctype == READ_MULTI)
      *s_mcsr |= 0x8000;  //enable read multiple

    ts_clock(&ts1);
    for(loop = tp.count; loop--; loop)
    {
      *s_mrar = tp.paddr;
      *s_mrtc = tp.size;
      while(*s_mrtc);
    }
    ts_clock(&ts2);
  }
  else
  {  
    *s_mcsr = AMCC_WRITE_ENABLE;

    ts_clock(&ts1);
    for(loop = tp.count; loop--; loop)
    {
      *s_mwar = tp.paddr;
      *s_mwtc = tp.size;
      while(*s_mwtc);
    }
    ts_clock(&ts2);
  }

  atime = ts_duration(ts1, ts2);
  *mbps = tp.size * tp.count / atime / 1024.0 / 1024.0;

  return(0);
}


/****************/
int abench32(void)
/****************/
{
  float atime, mbpsr, mbpsw, mbps[9]; 
  tparams tp;
  char dummy[2];
  u_int loop, data;
  tstamp ts1, ts2;
  
  tp.paddr = paddr[1];
  
  printf("Put the switch of the 32 bit PCI BLASTER into the\n");
  printf("WRITE position and press <return>\n");
  fgets(dummy, 2, stdin);
  tp.dir = DMA_WRITE; tp.size  = 100;   tp.count = 1;   tp.ctype = WRITE;      dodma32(tp, &mbps[0]);
  tp.dir = DMA_WRITE; tp.size  = 10000; tp.count = 1;   tp.ctype = WRITE;      dodma32(tp, &mbps[1]);
  tp.dir = DMA_WRITE; tp.size  = 100;   tp.count = 100; tp.ctype = WRITE;      dodma32(tp, &mbps[2]);

  
  printf("Put the switch of the 32 bit PCI BLASTER into the\n");
  printf("READ position and press <return>\n");
  fgets(dummy, 2, stdin);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 1;   tp.ctype = READ;       dodma32(tp, &mbps[3]);
  tp.dir = DMA_READ;  tp.size  = 10000; tp.count = 1;   tp.ctype = READ;       dodma32(tp, &mbps[4]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 100; tp.ctype = READ;       dodma32(tp, &mbps[5]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 1;   tp.ctype = READ_MULTI; dodma32(tp, &mbps[6]);
  tp.dir = DMA_READ;  tp.size  = 10000; tp.count = 1;   tp.ctype = READ_MULTI; dodma32(tp, &mbps[7]);
  tp.dir = DMA_READ;  tp.size  = 100;   tp.count = 100; tp.ctype = READ_MULTI; dodma32(tp, &mbps[8]);

  
  printf("\n\n=========================================================================\n");
  printf("All results in MB/s | 1 * 100 bytes  | 1 * 10000 bytes  | 100 * 100 bytes\n");
  printf("====================|================|==================|================\n");
  printf("Write               |      %9.1f |        %9.1f |      %9.1f \n", mbps[0], mbps[1], mbps[2]);
  printf("Read                |      %9.1f |        %9.1f |      %9.1f \n", mbps[3], mbps[3], mbps[5]);
  printf("Read multiple       |      %9.1f |        %9.1f |      %9.1f \n", mbps[6], mbps[7], mbps[8]);
  printf("=========================================================================\n");
  
  ts_clock(&ts1);
  for(loop = 1000; loop--; loop)
    data = *s_mwar;      
  ts_clock(&ts2);
  atime = ts_duration(ts1, ts2);
  mbpsr = 4000 / atime / 1024.0 / 1024.0;

  ts_clock(&ts1);
  for(loop = 1000; loop--; loop)
    *s_mwar = 0;
  ts_clock(&ts2);
  atime = ts_duration(ts1, ts2);
  mbpsw = 4000 / atime / 1024.0 / 1024.0;
  
  printf("\n");
  printf("PCI single cycles   |        32 bit |\n");
  printf("====================|===============|\n");
  printf("Write               |     %9.1f |\n", mbpsw);
  printf("Read                |     %9.1f |\n", mbpsr);
  
  return(0);
}


/*******************/
int readbench32(void)
/*******************/
{
  static u_int mode = 1, rtype = 1, size = 0x100, count = 100;
  int loop;
  tstamp ts1, ts2;
  float atime, mbps;
 
  printf("\n\n==============================================================\n");
  printf("Set swith of 32 bit PCI-BLASTER to <read>\n");
  printf("PCI address = 0x%016lx\n",paddr[1]);
   
  printf("Do you want to use polling (1=yes  0=no): ");
  mode = getdecd(mode);

  printf("Select scycle type (1=read  2=read multiple) ");
  rtype = getdecd(rtype);

  if (mode == 1)
  {
    printf("Enter the size in bytes (max. 0x%08x): ", BUFSIZE);
    size = gethexd(size);

    printf("Enter the loop count: ");
    count = getdecd(count);
  }
  else
    size = 0x1000;
  
  ts_clock(&ts1);
  *s_mcsr = AMCC_READ_ENABLE;
  if (rtype == 2)
    *s_mcsr |= 0x8000;  //enable read multiple

  if (!mode)
  {
    *s_mrar = paddr[1];
    *s_mrtc = size;
  }
  else
  {
    for(loop = count; loop--; loop)
    {
      *s_mrar = paddr[1];
      *s_mrtc = size;
      while(*s_mrtc);
    }
    ts_clock(&ts2);
  
    atime = ts_duration(ts1, ts2);
    printf("\nTimer value:        %f s\n", atime);
    printf("Total transfer size   %d bytes\n", size * count);
    mbps = size * count / atime / 1024.0 / 1024.0;
    printf("Performance (write)   %f MB/s\n",mbps);
  }
  printf("==============================================================\n");
  return(0);
}


/********************/
int writebench32(void)
/********************/
{
  static u_int mode = 1, size = 0x100, count = 100;
  int loop;
  tstamp ts1, ts2;
  float atime, mbps;
  
  printf("\n\n==============================================================\n");
  printf("Set swith of 32 bit PCI-BLASTER to <write>\n");
  printf("PCI address = 0x%016lx\n",paddr[0]);

  printf("Do you want to use polling (1=yes  0=no): ");
  mode = getdecd(mode);

  if (mode == 1)
  {
    printf("Enter the size in bytes (max. 0x%08x): ",BUFSIZE);
    size = gethexd(size);

    printf("Enter the loop count: ");
    count = getdecd(count);
  }
  else
    size = 0x1000;
  
  ts_clock(&ts1);
  *s_mcsr = AMCC_WRITE_ENABLE;

  if (!mode)
  {
    *s_mwar = paddr[0];
    *s_mwtc = size;
  }
  else
  {
    for(loop = count; loop--; loop)
    {
      *s_mwar = paddr[0];
      *s_mwtc = size;
      while(*s_mwtc);
    }
    ts_clock(&ts2);
    
    atime = ts_duration(ts1, ts2);
    printf("\nTimer value:        %f us\n", atime*1000000.0);
    printf("Total transfer size   %d bytes\n", size * count);
    mbps = size * count / atime / 1024.0 / 1024.0;
    printf("Performance (write)   %f MB/s\n",mbps);
  }
  printf("==============================================================\n");
  return(0);
}


/****************/
int loadboth(void)
/****************/
{
  printf("to be done\n");
  return(0);
}


/************/
int sc32(void)
/************/
{
  printf("to be done\n");
  return(0);
}


/************/
int sc64(void)
/************/
{
  u_int size;

  size = pciblast->wsize;
  size = pciblast->wsize;
  size = pciblast->wsize;
  size = pciblast->wsize;
  size = pciblast->wsize;
  size = pciblast->wsize;

  pciblast->wsize = size;
  pciblast->wsize = size;
  pciblast->wsize = size;
  pciblast->wsize = size;
  pciblast->wsize = size;
  pciblast->wsize = size;

  size = pciblast->wsize;
  pciblast->wsize = size;
  size = pciblast->wsize;
  pciblast->wsize = size;
  size = pciblast->wsize;
  pciblast->wsize = size;
  return(0);
}


/****************/
int dumpconf(void)
/****************/
{
  printf("to be done (PCI driver required)\n");
  return(0);
}


/************/
int main(void)
/************/
{
  static int ret, fun = 1, num32, num64;

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;
  ret=sigaction(SIGQUIT, &sa, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }

  ts_open(1,TS_DUMMY);
  uio_init();
  
  printf("Enter occurence number of 64-bit PCIBLASTER (0 = no card) \n");
  num64 = getdecd(1);
  if (num64)
    s32pci64_map(num64);

  printf("Enter occurence number of 32-bit PCIBLASTER (0 = no card) \n");
  num32 = getdecd(0);
  if (num32)    
    s32pci32_map(num32);

  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf(" 1 Print help                         2 Dump PCI MEM registers\n");
    printf(" 3 Reset the PCI blaster              4 Load both buses\n");   
    if (have32)
      printf(" 5 Benchmark 32 bit PCI read          6 Benchmark 32 bit PCI write\n");
    if (have64)
      printf(" 7 Benchmark 64 bit PCI read          8 Benchmark 64 bit PCI write\n");
    if (have32)
      printf(" 9 Benchmark single cycles 32 bit    10 Auto-benchmark 32 bit\n");
    if (have64)
    {
      printf("11 Benchmark single cycles 64 bit    12 Auto-benchmark 64 bit\n");      
      printf("13 Loop on 64 bit read               14 Loop on 64 bit write\n");
    }
    printf(" 0 Quit\n");
    printf("Your choice ");
    fun=getdecd(fun);
    if (fun==1) mainhelp();
    if (fun==2) dumpmem();
    if (fun==3) cardreset();
    if (fun==4) loadboth();
    if (fun==5) readbench32();
    if (fun==6) writebench32();
    if (fun==7) readbench();
    if (fun==8) writebench();
    if (fun==9) sc32();
    if (fun==10) abench32();
    if (fun==11) sc64();
    if (fun==12) abench();
    if (fun==13) loop64r();
    if (fun==14) loop64w();
  }

  if (num64)    
    s32pci64_unmap();
  if (num32)    
    s32pci32_unmap();
  uio_exit();
  ts_close(TS_DUMMY);
  exit(0);
}







