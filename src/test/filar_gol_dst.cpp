/*****************************************************************************/
/*                                                                           */
/* File Name        : filar_gol_dst.cpp                                      */
/*                                                                           */
/* Author           : Markus Joos			                     */
/*                                                                           */
/***** C 2004 - A nickel program worth a dime ********************************/

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "ROSfilar/filar.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "rcc_time_stamp/tstamp.h"
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>
#include <pmg/pmg_initSync.h>

using namespace ROS;
using namespace daq::tmgr;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

// global variables
int verbose = FALSE;
int print = FALSE;
int dcheck = TRUE;
int L1extdcheck = TRUE; 
int npackets = -1;
int occurence = 1;
int psize = 4;
int usdelay = 0;
int test_status = TmPass;
int writefile = FALSE;
int outputFile;
char filename[200] = {0};
int checkfile = FALSE;
int inputFile;
char filename2[200] = {0};
int rawdump = 0, dev, flg_error, ipacket, lpacket, delta_time, page_size, packet_size;
long start_time,end_time;
float t_per_packet, packet_per_s, mb_per_s;
u_int rec_size,exp_size, last_l1id = 999999;
char refbuf[0x100000];
u_int dblevel = 0, dbpackage = DFDB_ROSFILAR;
bool DVS = FALSE;

typedef enum printType { SUMMARY=0 , FULL=1 } PrintType;
typedef enum checkType { SIZE , L1ID , GLOBAL } CheckType;

CheckType ccode;
int errors[GLOBAL+1] = { GLOBAL * 0 , 0 };
int errorsTot[GLOBAL+1] = { GLOBAL * 0 , 0 };

// Constants
#define MAX_PACKET_SIZE_WRDS 2048 


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Occurence                                    -> Default: " << occurence << std::endl;
  std::cout << "-p  : FILAR page size                              -> Default: 16 kB" << std::endl;
  std::cout << "      Encoding:" << std::endl;
  std::cout << "      -p 0 = 256 bytes:" << std::endl;
  std::cout << "      -p 1 =   1 kbyte:" << std::endl;
  std::cout << "      -p 2 =   2 kbyte:" << std::endl;
  std::cout << "      -p 3 =   4 kbyte:" << std::endl;
  std::cout << "      -p 4 =  16 kbyte:" << std::endl;
  std::cout << "      -p 5 =  64 kbyte:" << std::endl;
  std::cout << "      -p 6 = 256 kbyte:" << std::endl;
  std::cout << "      -p 7 =   4 Mbyte:" << std::endl;
  std::cout << "-n x: Number of packets                            -> Default: infinite" << std::endl;
  std::cout << "-f x: Write data to file. The parameter is the path and the name of the file" << std::endl;
  std::cout << "-C x: Compare with data from file. The parameter is the path and the name of the reference file" << std::endl;
  std::cout << "-v  : Verbose output                               -> Default: FALSE" << std::endl;
  std::cout << "-r x: Number of raw data words to be dumped        -> Default: 0" << std::endl;
  std::cout << "-w x: Number of microseconds to wait after" << std::endl;
  std::cout << "      the receptionof a packet                     -> Default: 0" << std::endl;  
  std::cout << "-d  : Debug level                                  -> Default: 0" << std::endl;
  std::cout << "-D  : Debug package                                -> Default: FILAR" << std::endl;
  std::cout << std::endl;
}


/***************************/
void printStat(PrintType arg) 
/***************************/
{
  std::cout << std::endl << std::endl << "----------Test Statistics-----------" << std::endl;

  if (arg) 
  {
    std::cout << " # packets transferred       = " << (ipacket - lpacket) << std::endl;
    std::cout << " # corrupted packets         = " << errors[GLOBAL] << std::endl;
    std::cout << "      wrong size             = " << errors[SIZE] << std::endl;
    std::cout << "      wrong L1id             = " << errors[L1ID] << std::endl;
    std::cout << " time elapsed (s)            = " << delta_time << std::endl;
    std::cout << "------------------------------------" << std::endl;
  }
  
  std::cout << " Total # packets transferred = " << (ipacket-1) << std::endl;
  std::cout << " Total # corrupted packets   = " << errorsTot[GLOBAL] << std::endl;
  std::cout << "            wrong size       = " << errorsTot[SIZE] << std::endl;
  std::cout << "            wrong L1id       = " << errorsTot[L1ID] << std::endl;
  
  if (arg && (ipacket - lpacket)) 
  {    
    t_per_packet = ((float) delta_time * 1000000) / (ipacket - lpacket);
    packet_per_s = (float)1000000 / t_per_packet;
    mb_per_s = ((float)(ipacket - lpacket) * packet_size * 4) / ((float)delta_time * 1024 * 1024);
    std::cout << "------------------------------------" << std::endl;
    std::cout << " time/packet = " << t_per_packet << " microseconds" << std::endl;
    std::cout << " packet/s    = " << packet_per_s << " packet/s" << std::endl;
    std::cout << " Mbyte/s     = " << mb_per_s << std::endl;
  }

  std::cout << "------------------------------------" << std::endl << std::endl;
}


/*****************************/
void errorFound(CheckType type) 
/*****************************/
{
  if (!flg_error) 
  {
    errors[GLOBAL]++;
    errorsTot[GLOBAL]++;
  }
  flg_error = 1;
  errors[type]++;
  errorsTot[type]++;
  test_status = TmFail;
}


/*********************/
void terminate_it(void) 
/*********************/
{
  err_type code;

  std::cout << std::endl << std::endl <<"Closing S-link ..." << std::endl;
  code = FILAR_Reset(dev);
  if (code)
  {
    std::cout << "main: FILAR_Reset() FAILED with error " << HEX(code) << std::endl;
    rcc_error_print(stdout, code);
  }
  
  code = FILAR_Close();
  if (code)
  {
    std::cout << "main: FILAR_Close() FAILED with error " << HEX(code) << std::endl;
    rcc_error_print(stdout, code);
  }

  code = ts_close(TS_DUMMY);
  if (code) 
    rcc_error_print(stdout, code);

  if (writefile)
    close (outputFile);

  printStat(SUMMARY);
}


// sigquit handler
/******************************/
void sigquit_handler(int signum)
/******************************/
{
  int i;
  
  if (start_time) 
  {
    time(&end_time);
    delta_time = end_time - start_time;
    if (delta_time) 
      printStat(FULL);
  }
  
  // reset flags
  lpacket = ipacket;
  for (i = 0;i < GLOBAL; i++) {errors[i] = 0;}
  time(&start_time);
}


// sigint handler
/*****************************/
void sigint_handler(int signum)
/*****************************/
{
  terminate_it();
  exit(TmPass);
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  FILAR_config_t fconfig;
  FILAR_in_t fin;
  FILAR_out_t fout;
  MemoryPage *mem_page;
  MemoryPool *m_memoryPool;
  u_int code;
  struct sigaction sa, saint;
  int c;
  u_int maxdump, loop, virt_addr, pci_addr;

  static struct option long_options[] = {"DVS", no_argument, NULL, '1'}; 
 
  while ((c = getopt_long(argc, argv, "p:ho:n:vf:C:d:D:r:w:1", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(TmUnresolved);
      break;

    case 'o':
      occurence = atoi(optarg);
      if (occurence <= 0 || occurence > MAXROLS)
      { 
	std::cout << "Occurence exceeds allowed bounds" << std::endl;
        exit(TmUnresolved);
      }
      break;

    case 'n':
      npackets = atoi(optarg);
      if (npackets<0) 
      {
	std::cout << "number of packets must be positive" << std::endl;
	exit(TmUnresolved);
      } 
      break;
                  
    case 'f':   
      writefile = TRUE;
      sscanf(optarg,"%s",filename);
      std::cout << "writing data to file " << filename << std::endl;
      break;
      
     case 'C':   
      checkfile = TRUE;
      sscanf(optarg, "%s", filename2);
      std::cout << "Reading reference fragment from file " << filename2 << std::endl;
      break;
   
    case 'r': rawdump = atoi(optarg);   break; 
    case 'p': psize = atoi(optarg);     break; 
    case 'd': dblevel = atoi(optarg);   break;
    case 'D': dbpackage = atoi(optarg); break;                   
    case 'w': usdelay = atoi(optarg);   break;                   
    case 'v': verbose = TRUE;           break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (TmUnresolved);
    }

  // Initialize the debug macro
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);      
      
  // Install signal handler for SIGQUIT & SIGINT
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }

  code = ts_open(0, TS_DUMMY);
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }

  /* Install signal handler for SIGINT */
  sigemptyset(&saint.sa_mask);
  saint.sa_flags   = 0;
  saint.sa_handler = sigint_handler;

  if (sigaction(SIGINT, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }
  
  code = FILAR_Open();
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }
  
  dev = (occurence - 1);
  //Initialize
  for(loop = 0; loop < MAXROLS; loop++)
    fconfig.enable[loop] = 0;
  fconfig.enable[dev] = 1;
  fconfig.psize = psize;
  fconfig.bswap = 0;          //Not yet supported
  fconfig.wswap = 0;          //Not yet supported
  fconfig.scw = 0xb0f0000;    //Not yet used
  fconfig.ecw = 0xe0f0000;    //Not yet used      

  code = FILAR_Reset(dev);
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }

  code = FILAR_Init(&fconfig);
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }
  
  if (verbose)
    std::cout << "Resetting the link ....." << std::endl;
  code = FILAR_LinkReset(dev);
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }
    
  if (verbose)
    std::cout << ".... reset complete." << std::endl;

  if (verbose) 
  {
    std::cout << "FILAR parameters structure : " << std::endl;
    std::cout << "page size(encoded) = " << fconfig.psize << std::endl;
    std::cout << "active channel     = " << dev << std::endl;
    std::cout << "start_word         = 0x" << HEX(fconfig.scw) << std::endl;
    std::cout << "stop_word          = 0x" << HEX(fconfig.ecw) << std::endl;
  }
 
  //Create the Memory Pool
  m_memoryPool = new MemoryPool_CMEM(10, MAX_PACKET_SIZE_WRDS * sizeof(int));
  
  //Get a page
  try
  {
    mem_page = m_memoryPool->getPage();  //MJ: Check for error!!
  }
  catch (MemoryPoolException& e)
  {
    std::cout << e;
    exit (TmFail);
  }
  pci_addr = mem_page->physicalAddress();
  virt_addr = (intptr_t)mem_page->address();
  page_size = mem_page->capacity();

  if (verbose) 
  {
    std::cout << "Printing buffer parameters:" << std::endl; 
    std::cout << "PCI address of data buffer     = 0x" << HEX(pci_addr) << std::endl;
    std::cout << "Virtual address of data buffer = 0x" << HEX(virt_addr) << std::endl;
    std::cout << "Buffer size in bytes           = " << mem_page->capacity() << std::endl;
  }
  
  // Open the file for output
  if (writefile)
  {
    outputFile = open(filename, O_WRONLY|O_CREAT|O_LARGEFILE, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if (outputFile < 0)
    {
      std::cout << "Failed to open " << filename << std::endl;
      exit(TmUnresolved);
    }
  }
        
  // Open the reference file
  if (checkfile)
  {
    inputFile = open(filename2, O_RDONLY);
    if (inputFile < 0)
    {
      std::cout << "Failed to open " << filename2 << std::endl;
      exit(TmUnresolved);
    }

    int refsize = read(inputFile, refbuf, 0x100000);
    std::cout << "The reference file contains " << refsize << " bytes" << std::endl;
    if (refsize < 44)
    {
      std::cout << "The reference file contains less than 44 bytes (i.e. ROD header + trailer)" << std::endl;
      exit(TmUnresolved);
    }
    if (refsize == 0x100000)
    {
      std::cout << "The reference file contains more than 1 MB" << std::endl;
      exit(TmUnresolved);
    }
    close(inputFile);
  }
  
  time(&start_time);
  lpacket = 1;

  std::cout << std::endl << "Press ctrl-\\ to output statistics" << std::endl;
  std::cout << "Press ctrl-c to quit" << std::endl << std::endl;
  
  ipacket = 1;
  while(ipacket <= ((npackets >= 0) ? npackets : (ipacket))) 
  {
    fin.nvalid = 1;      
    fin.channel = dev;
    fin.pciaddr[0] = pci_addr;
    code = FILAR_PagesIn(&fin);
    if (code) 
    {
      rcc_error_print(stdout, code);
      exit (TmFail);
    }  

    if(verbose)
    {
      std::cout << "FILAR_PagesIn called with:" << std::endl;
      std::cout << "pci_addr = 0x" << HEX((u_int) pci_addr) << std::endl;
    }
    fout.nvalid = 0;
    while (!fout.nvalid)
    {
      code = FILAR_Flush();
      if (code) 
      {
       std::cout << "main: FILAR_Flush() FAILED with " << HEX(code) << std::endl;
       rcc_error_print(stdout, code);
       exit(TmFail);
      }

      code = FILAR_PagesOut(dev, &fout);
      if (code) 
      {
	std::cout << "main: FILAR_PagesOut() FAILED with " << HEX(code) << std::endl;
	rcc_error_print(stdout, code);
	exit(TmFail);
      }
    }

    if (fout.fragstat[0])
      std::cout << " ERROR in S-link packet. FILAR status word = " << fout.fragstat[0] << std::endl;

    packet_size = fout.fragsize[0]; // We need it in words
    if (!packet_size)
      std::cout << " Empty packet received" << std::endl;
    if (verbose) 
      std::cout << "Packet size received = " << packet_size << " words" << std::endl;  


    if (rawdump)
    {
      if (packet_size > rawdump)
        maxdump = rawdump;
      else
        maxdump = packet_size;

      std::cout << "Dumping the first " << maxdump << " words of the ROD data" << std::endl;
      u_int *dataptr = (u_int *)virt_addr;
      for(loop = 0; loop < maxdump; loop++)
        std::cout << "Word " << loop << " = 0x" << HEX(*dataptr++) << std::endl;
    }

    if (verbose)
        std::cout << "writefile = " << writefile << std::endl; 
    if (writefile)
    {
      if (verbose)
        std::cout << "writing " << packet_size * 4 << " bytes to file" << std::endl; 
      int isok = write(outputFile, (void *)virt_addr, packet_size * 4);
      if (isok < 0)
      {
        std::cout << "Error in writing to file" << std::endl;
        exit(TmUnresolved);
      }
    }

    if (checkfile)
    {
      u_int *refptr = (u_int *)refbuf;
      u_int *dataptr = (u_int *)mem_page->address();
      for (int dataword = 0; dataword < packet_size; dataword++)
      {
        if (*refptr != *dataptr)
          std::cout << "Data mismatch: Offset = 0x" << HEX(dataword * 4) << " Received data = 0x" << HEX(*dataptr) << "   Expected data = 0x" << HEX(*refptr) << std::endl;
        refptr++;
        dataptr++;
      }
    }

    ipacket++;
    
    if (usdelay)
      ts_delay(usdelay);
  }
  terminate_it();
  exit(TmPass); 
}

