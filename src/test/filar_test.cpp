/************************************************************************/
/*									*/
/* File: filar_test.c							*/
/*									*/
/* This is the test program for the ROSfilar package 			*/
/*									*/
/*  7. Jun. 02  MAJO  created						*/
/*									*/
/**************** C 2002 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSfilar/filar.h"
#include "ROSGetInput/get_input.h"

//Globals
u_int amode = 0, cont, gotalarm, cactive[MAXROLS], received[MAXROLS] ={0}, delta[MAXROLS] ={0};
u_long virtbase, pcibase;
static struct sigaction sa, sa2;
FILAR_info_t finfo;
u_int dblevel = 0, dbpackage = DFDB_ROSFILAR;

//Prototypes
int statistics(void);
int mainhelp(void);
int func_menu(void);
int auto_menu(void);
int man_menu(void);
int slidastest(void);
int slidastest2(void);
int slidastest3(void);
int slidasdump(void);
int setdebug(void);

#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
  debug(("SigQuitHandler: ctrl+// received\n"));
  cont = 0;
  alarm(0);
}


/***************************/
void AlarmHandler(int signum)
/***************************/
{
  gotalarm = 1;
}


/******************/
int statistics(void)
/******************/
{
  u_int channel;

  gotalarm = 0;

  printf("Number of fragments received:\n");
  for(channel = 0; channel < MAXROLS; channel++)
  {
    if (cactive[channel])
    {
      received[channel] += delta[channel];
      printf("Channel %d: total #=%d    # per second=%d\n", channel, received[channel], delta[channel]/2);
      delta[channel] = 0;
    }
  }

  if (amode)
    FILAR_Flush();
  
  if (cont)
    alarm(2);
return(0);
}


/************/
int main(void)
/************/
{
  int fun = 3;
  int ret;
  int memhandle;
  
  printf("\n\n\nThis is the test program for the FILAR library \n");
  printf("===================================================\n");

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;
  ret = sigaction(SIGQUIT, &sa, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = AlarmHandler;
  ret = sigaction(SIGALRM, &sa2, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  ret = CMEM_SegmentAllocate(0x100000, "FILAR_BUFFER", &memhandle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ret = CMEM_SegmentVirtualAddress(memhandle, &virtbase);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(memhandle, &pcibase);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("Contiguous buffer:\n");
  printf("Physical address = 0x%016lx\n", pcibase);
  printf("Virtual address  = 0x%016lx\n", virtbase);

  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                        2 Test functions\n");
    printf("   3 Automatic tests             4 Manual tests\n");
    printf("   5 Set debug parameters\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) func_menu();
    if (fun == 3) auto_menu();
    if (fun == 4) man_menu();
    if (fun == 5) setdebug();
  }

  ret = CMEM_SegmentFree(memhandle);
  if (ret)
    rcc_error_print(stdout, ret);
 
  ret = CMEM_Close();
  if (ret)
    rcc_error_print(stdout, ret);
   
  return(0);
}


/****************/
int setdebug(void)
/****************/
{
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
int auto_menu(void)
/*****************/
{
  int fun = 1;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a test:\n");
    printf("   1 Handshake up to %d channels (fast)\n", MAXROLS);
    printf("   2 Handshake up to %d channels (slow)\n", MAXROLS);    
    printf("   3 Handshake 1 channel (ultra fast)\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);    
    if (fun == 1) slidastest();
    if (fun == 2) slidastest2();
    if (fun == 3) slidastest3();
  }
  printf("=========================================================================\n\n");
  return(0);
}


/****************/
int man_menu(void)
/****************/
{
  int fun = 1;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a test:\n");
    printf("   1 Dump data from one channel\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);    
    if (fun == 1) slidasdump();
  }
  printf("=========================================================================\n\n");
  return(0);
}


/******************/
int slidasdump(void)
/******************/
{      
  FILAR_ErrorCode_t ret;
  FILAR_config_t fconfig;
  FILAR_in_t fin;
  FILAR_out_t fout;
  u_int *ptr, loop, channel;
  static u_int cnum = 0;
 
  printf("Enter the channel number (0..%d): ", MAXROLS);
  cnum = getdecd(cnum);
 
  ret = FILAR_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  ret = FILAR_Reset(cnum);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  ret = FILAR_LinkReset(cnum);   
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  for(channel = 0; channel < MAXROLS; channel++)
    fconfig.enable[channel] = 0; 
    
  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.scw = 0xb0f00000;
  fconfig.ecw = 0xe0f00000; 
  fconfig.enable[cnum] = 1; 
  fconfig.psize = 4;
  fconfig.interrupt = 1;  

  ret = FILAR_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  } 
  
  cont = 1;
  for(channel = 0; channel < MAXROLS; channel++)
    cactive[channel] = 0;
  cactive[cnum] = 1;

  while(cont)
  {
    // Fill the IN FIFO with one entry
    fin.nvalid = 1; 
    fin.channel = cnum;
    fin.pciaddr[0] = pcibase;  //we use the same page buffer all the time        
    ret = FILAR_PagesIn(&fin);
    if(ret != 0)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }

    channel = cnum;
    fout.nvalid = 0;
    printf("Waiting for S-Link packet....\n");
    while(fout.nvalid == 0)
    {
      // Flush the driver. As we want to handle single fragment we can 
      // not wait for the interrupt from the FILAR
      ret = FILAR_Flush();   
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    
      // Check if a package has arrived   
      ret = FILAR_PagesOut(channel, &fout);
      if (ret)
      {
	rcc_error_print(stdout, ret);
        return(-1);
      }
    }
    
    printf("S-Link packet received. Dumping data...\n");
    printf(" PCI address     = 0x%08x\n", fout.pciaddr[0]);
    printf(" Fragment size   = 0x%08x\n", fout.fragsize[0]);
    printf(" Fragment status = 0x%08x\n", fout.fragstat[0]);
    ptr = (u_int *) virtbase;
    for(loop = 0; loop < fout.fragsize[0]; loop++)
      printf("Offset = 0x%04x   Data = 0x%08x\n", loop, *ptr++);
      
    printf("Continue (1=yes 0=no)\n");
    cont = getdecd(cont);  
  }
 
  ret = FILAR_Close();
  if (ret)
    rcc_error_print(stdout, ret);
  return(0);
}


/******************/
int slidastest(void)
/******************/
{      
  FILAR_ErrorCode_t ret;
  FILAR_config_t fconfig;
  FILAR_in_t fin;
  FILAR_out_t fout;
  static u_int cerr = 1, nchannels = 4;
  u_int nfree, nmin, loop, data, maxbuffers[MAXROLS], channel;
    
  ret = FILAR_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  printf("Enter the number of channels: ");
  nchannels = getdecd((int)nchannels);

  for(channel = 0; channel < MAXROLS; channel++)
    cactive[channel] = 0;

  for(channel = 0; channel < nchannels; channel++)
  {
    printf("Enter the number of channel %d: ", channel + 1);
    data = getdecd(channel);
    cactive[data] = 1;
  }

  for(channel = 0; channel < MAXROLS; channel++)
  {
    if (cactive[channel])
    {
      ret = FILAR_Reset(channel);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
  }
    
  for(channel = 0; channel < MAXROLS; channel++)
    fconfig.enable[channel] = 0; 

  fconfig.psize = 0;
  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.interrupt = 0;
  fconfig.scw = 0xb0f00000;
  fconfig.ecw = 0xe0f00000;
  
  for(channel = 0; channel < MAXROLS; channel++)
  {
    if (cactive[channel])
    {
      fconfig.enable[channel] = 1;
      printf("Channel %d enabled\n", channel);
      
      ret = FILAR_LinkReset(channel);   
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
  }

  printf("Select Page size\n");
  printf(" 0 = 256 bytes\n");
  printf(" 1 =   1 Kbyte\n");
  printf(" 2 =   2 Kbytes\n");
  printf(" 3 =   4 Kbytes\n");
  printf(" 4 =  16 Kbytes\n");
  printf(" Your choice: ");
  fconfig.psize = getdecd(4);
  fconfig.interrupt = 1;
  
  printf("Output control words  (2=on error  1=allways 0=no): ");
  fconfig.ccw = getdecd(0);

  printf("Display transmission errors  (1=yes  0=no): ");
  cerr = getdecd(cerr);

  ret = FILAR_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  } 
  
  cont = 1;
  amode = 1;
  gotalarm = 0;
  alarm(2);
  printf("Press crtl+\\ to stop this test\n");
      
  for(channel = 0; channel < MAXROLS; channel++)
    maxbuffers[channel] = MAXOUTFIFO - 10; // we do not want to overflow the OUT FIFO
  
  ret = FILAR_Info(&finfo);  // needed later
  if (ret)
    rcc_error_print(stdout, ret);

  while(cont)
  {
    for(channel = 0; channel < MAXROLS; channel++)
    {
      if (finfo.channels[channel] && finfo.enabled[channel])
      {
        //Fill the IN FIFO to the top but do not write more entries than the OUT FIFO can digest
        ret = FILAR_InFree(channel, &nfree);
        if (ret)
        {
          rcc_error_print(stdout, ret);
	  alarm(0);
          return(-1);
        }
        if (nfree < maxbuffers[channel])
          nmin = nfree;
        else
          nmin = maxbuffers[channel];
	  
        if (nmin)
        {	  
          fin.nvalid = nmin;
          fin.channel = channel;
          for (loop = 0; loop < nmin; loop++)
            fin.pciaddr[loop] = pcibase + 0x1000 * channel;  //we use the same page buffer all the time
          maxbuffers[channel] -= nmin;

          ret = FILAR_PagesIn(&fin);
          if (ret)
          {
            rcc_error_print(stdout, ret);
            alarm(0);
            return(-1);
          }
        }
      }
    }

    for(channel = 0; channel < MAXROLS; channel++)
    {
      if (finfo.channels[channel] && finfo.enabled[channel])
      {
        fout.nvalid = 0;
        ret = FILAR_PagesOut(channel, &fout);
        if (ret)
        {
	  rcc_error_print(stdout, ret);
          alarm(0);
          return(-1);
        }

        if (cerr)
        {
          for(loop = 0; loop < fout.nvalid; loop++)
          {
            if (fout.fragstat[loop])
	    {
              printf("Fragment %d returns status 0x%08x\n", loop, fout.fragstat[loop]);
	      if (fconfig.ccw == 2)
		printf("Fragment %d has SCW = 0x%08x and ECW = 0x%08x\n", loop, fout.scw[loop], fout.ecw[loop]);
	    }
	      
          }
        }
	
	if (fconfig.ccw == 1)
	{          
	  for(loop = 0; loop < fout.nvalid; loop++)
	    printf("Fragment %d has SCW = 0x%08x and ECW = 0x%08x\n", loop, fout.scw[loop], fout.ecw[loop]);
	}
	
	if (gotalarm)
  	  statistics();
        delta[channel] += fout.nvalid;
        maxbuffers[channel] += fout.nvalid;
      }
    }
  }
  
  amode = 0;
  alarm(0);
  ret = FILAR_Close();
  if (ret)
    rcc_error_print(stdout, ret);
  return(0);
}


/*******************/
int slidastest2(void)
/*******************/
{      
  FILAR_ErrorCode_t ret;
  FILAR_config_t fconfig;
  FILAR_in_t fin;
  FILAR_out_t fout;
  static u_int trace = 0, nchannels = 2, dotest = 1;
  u_int *ptr, data, channel, offset, first[MAXROLS], lastl1id[MAXROLS];
    
  ret = FILAR_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  printf("Enter the number of channels: ");
  nchannels = getdecd((int)nchannels);

  for(channel = 0; channel < MAXROLS; channel++)
    cactive[channel] = 0;

  for(channel = 0; channel < nchannels; channel++)
  {
    printf("Enter the number of channel %d: ", channel + 1);
    data = getdecd(channel);
    cactive[data] = 1;
  }

  for(channel = 0; channel < MAXROLS; channel++)
  {
    if (cactive[channel])
    {
      ret = FILAR_Reset(channel);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
  }
    
  for(channel = 0; channel < MAXROLS; channel++)
    fconfig.enable[channel] = 0; 

  fconfig.psize = 0;
  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.interrupt = 0;
  fconfig.scw = 0xb0f00000;
  fconfig.ecw = 0xe0f00000;
  
  for(channel = 0; channel < MAXROLS; channel++)
  {
    if (cactive[channel])
    {
      fconfig.enable[channel] = 1;
      printf("Channel %d enabled\n", channel);
      
      ret = FILAR_LinkReset(channel);   
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
  }

  printf("Select Page size\n");
  printf(" 0 = 256 bytes\n");
  printf(" 1 =   1 Kbyte\n");
  printf(" 2 =   2 Kbytes\n");
  printf(" 3 =   4 Kbytes\n");
  printf(" 4 =  16 Kbytes\n");
  printf(" 5 =  64 Kbytes\n");
  printf(" Your choice: ");
  fconfig.psize = getdecd(4);
  fconfig.interrupt = 1;
  if      (fconfig.psize == 0) offset = 0x100;
  else if (fconfig.psize == 1) offset = 0x400;
  else if (fconfig.psize == 2) offset = 0x800;
  else if (fconfig.psize == 3) offset = 0x1000;
  else if (fconfig.psize == 4) offset = 0x4000;
  else                         offset = 0x10000;


  ret = FILAR_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  } 

  printf("Print tracing information (1=yes  0=no) ");
  trace = getdecd(trace);
  
  printf("Check data (L1ID consistency) (1=yes  0=no) ");
  dotest = getdecd(dotest);
  
  cont = 1;
  amode = 0;
  alarm(2);
  
  for(channel = 0; channel < MAXROLS; channel++)
    first[channel] = 1;
  
  printf("Press crtl+\\ to stop this test\n");
  
  ret = FILAR_Info(&finfo);  // needed later
  if (ret)
    rcc_error_print(stdout, ret);

  while(cont)
  {
    for(channel = 0; channel < MAXROLS; channel++)
    {
      if (finfo.channels[channel] && finfo.enabled[channel])
      {
        //Fill the IN FIFO with one PCI address
        fin.nvalid = 1;
        fin.channel = channel;
        fin.pciaddr[0] = pcibase + offset * channel;  //we use the same page buffer all the time
        ret = FILAR_PagesIn(&fin);
        if (ret)
        {
          rcc_error_print(stdout, ret);
          alarm(0);
          return(-1);
        }
      }
    }

    for(channel = 0; channel < MAXROLS; channel++)
    {
      if (finfo.channels[channel] && finfo.enabled[channel])
      {
        //Wait for one fragment to arrive      
        fout.nvalid = 0;
        while(fout.nvalid == 0)
        {
          ret = FILAR_Flush();   
          if (ret)
          {
            rcc_error_print(stdout, ret);
            alarm(0);
            return(-1);
          }
          
          ret = FILAR_PagesOut(channel, &fout);
          if (ret)
          {
	    rcc_error_print(stdout, ret);
            alarm(0);
            return(-1);
          }
        }

        if (fout.fragstat[0])
	{
          printf("Fragment from channel %d returns status 0x%08x\n", channel, fout.fragstat[0]);
	  printf("fout.nvalid = %d\n",fout.nvalid); 
        }  

        // Perform some tests on the fragment
        if (dotest)
	{
	  ptr = (u_int *)(virtbase + offset * channel);
	  if (first[channel])
	  {
	    lastl1id[channel] = ptr[5];
            if (trace)
              printf("Fist L1ID = %d received from channel %d\n", lastl1id[channel], channel);
	    first[channel] = 0;
	  }
	  else
	  {
	    if ((lastl1id[channel] + 1) != ptr[5])
	      printf("Current L1ID = %d   Last L1ID = %d\n", ptr[5], lastl1id[channel]);
            if (trace)
              printf("New L1ID = %d received from channel %d\n", ptr[5], channel);
	    lastl1id[channel] = ptr[5];
	  }
	}

        delta[channel] += fout.nvalid;
        if (trace)
          printf("Fragment %d received from channel %d\n", delta[channel] + received[channel], channel);
      }
    }
  }
 
  alarm(0);
  ret = FILAR_Close();
  if (ret)
    rcc_error_print(stdout, ret);
  return(0);
}


/*******************/
int slidastest3(void)
/*******************/
{      
  FILAR_ErrorCode_t ret;
  FILAR_config_t fconfig;
  FILAR_in_t fin;
  FILAR_out_t fout;
  u_int nfree, nmin, loop, maxbuffers, activechannel;
    
  ret = FILAR_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  printf("Enter the number of the channel: ");
  activechannel = getdecd(0);

  ret = FILAR_Reset(activechannel);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  ret = FILAR_LinkReset(activechannel);   
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
   
  for(loop = 0; loop < MAXROLS; loop++)
    fconfig.enable[loop] = 0; 

  fconfig.psize                 = 4;
  fconfig.interrupt             = 1;  
  fconfig.ccw                   = 0;
  fconfig.psize                 = 0;
  fconfig.bswap                 = 0;
  fconfig.wswap                 = 0;
  fconfig.interrupt             = 0;
  fconfig.scw                   = 0xb0f00000;
  fconfig.ecw                   = 0xe0f00000;
  fconfig.enable[activechannel] = 1;
  printf("Channel %d enabled\n", activechannel);

  ret = FILAR_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  } 
  
  cont = 1;
  printf("Press crtl+\\ to stop this test\n");
      
  maxbuffers = MAXOUTFIFO - 10; // we do not want to overflow the OUT FIFO
  printf("maxbuffers = %d\n", maxbuffers);
  
  ret = FILAR_Info(&finfo);  // needed later
  if (ret)
    rcc_error_print(stdout, ret);

  while(cont)
  {
    //Fill the IN FIFO to the top but do not write more entries than the OUT FIFO can digest
    ret = FILAR_InFree(activechannel, &nfree);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      alarm(0);
      return(-1);
    }
    if (nfree < maxbuffers)
      nmin = nfree;
    else
      nmin = maxbuffers;

    if (nmin)
    {	  
      fin.nvalid = nmin;
      fin.channel = activechannel;
      for (loop = 0; loop < nmin; loop++)
        fin.pciaddr[loop] = pcibase;  //we use the same page buffer all the time
      maxbuffers -= nmin;

      ret = FILAR_PagesIn(&fin);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        alarm(0);
        return(-1);
      }
////      printf("%d pages in\n", nmin);
    }

    ret = FILAR_PagesOut(activechannel, &fout);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      alarm(0);
      return(-1);
    }
    

////    for (loop = 0; loop < fout.nvalid; loop++)
////    {
////      printf("Size   of fragment %d = %d\n", loop, fout.fragsize[loop]); 
////      printf("Status of fragment %d = %d\n", loop, fout.fragstat[loop]); 
////    }
    
////    printf("%d pages out\n", fout.nvalid);
    maxbuffers += fout.nvalid;
  }
  
  ret = FILAR_Close();
  if (ret)
    rcc_error_print(stdout, ret);
  return(0);
}


/*****************/
int func_menu(void)
/*****************/
{
  static u_int nchannels = 4, nelem = 1, achannel = 0;
  u_int loop, channel, nfree, data;
  int fun = 1;
  FILAR_ErrorCode_t ret;
  FILAR_config_t fconfig;
  FILAR_in_t fin;
  FILAR_out_t fout;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:\n");
    printf("   1 FILAR_Open               2 FILAR_Close\n");
    printf("   3 FILAR_Init               4 FILAR_Info\n");
    printf("   5 FILAR_PagesIn            6 FILAR_PagesOut\n");
    printf("   7 FILAR_Reset              8 FILAR_InFree\n");
    printf("   9 FILAR_Flush             10 FILAR_LinkReset\n");
    printf("  11 FILAR_LinkStatus\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun); 
     
    if (fun == 1)
    {
      ret = FILAR_Open();
      if (ret)
	rcc_error_print(stdout, ret);
      ret = FILAR_Info(&finfo);  // needed later
      if (ret)
	rcc_error_print(stdout, ret);
    }
    
    if (fun == 2) 
    {
      ret = FILAR_Close();
      if (ret)
	rcc_error_print(stdout, ret);
    }    

    if (fun == 3) 
    {
      //Initialize
      for(loop = 0; loop < MAXROLS; loop++)
        fconfig.enable[loop] = 0;
      fconfig.psize = 4;

      printf("Enter the number of channels: ");
      nchannels = getdecd((int)nchannels);
      int chnum = 0;
      for(loop = 0; loop < nchannels; loop++)
      {
        printf("Enter channel number (0..%d) of ROL %d: ", MAXROLS - 1, loop);
        chnum = getdecd(chnum);
        fconfig.enable[chnum] = 1;
        chnum++;
      }
      printf("Select Page size\n");
      printf(" 0 = 256 bytes\n");
      printf(" 1 =   1 Kbyte\n");
      printf(" 2 =   2 Kbytes\n");
      printf(" 3 =   4 Kbytes\n");
      printf(" 4 =  16 Kbytes\n");
      printf(" Your choice: ");
      fconfig.psize = getdecd((int)fconfig.psize);
      fconfig.bswap = 0;  //Not yet supported
      fconfig.wswap = 0;  //Not yet supported
      fconfig.scw = 0xb0f00000;    //Not yet used
      fconfig.ecw = 0xe0f00000;    //Not yet used      
      
      ret = FILAR_Init(&fconfig);
      if (ret)
	rcc_error_print(stdout, ret);
    }
    
    if (fun == 4)
    {
      ret = FILAR_Info(&finfo);  
      if (ret)
	rcc_error_print(stdout, ret);
      else
      {  
        printf("There are %d ROL channels installed\n", finfo.nchannels);
        for(channel = 0; channel < MAXROLS; channel++)
          printf("Channel %d is %s and %s\n", channel, finfo.channels[channel]?"present":"absent", finfo.enabled[channel]?"enabled":"disabled");
      }
    }
    
    if (fun == 5)
    {
      printf("Enter the number of the channel (0..%d, %d=all): ", MAXROLS, MAXROLS + 1);
      achannel = getdecd((int)achannel);
      
      printf("Enter the number of entries to write to the IN FIFO: \n");
      nelem = getdecd((int)nelem);
      
      if (nelem > MAXINFIFO)
      {
        printf("You can not write more than %d entries\n", MAXINFIFO);
        return(-1);
      }
      
      fin.nvalid = nelem;      
      
      if (achannel < (MAXROLS + 1))
      {
        fin.channel = achannel;
      
        for(loop = 0; loop < nelem; loop++)
          fin.pciaddr[loop] = pcibase + 0x1000 * (loop & 0x3);  //we use 4 single page buffers in a cyclic manner
      
        ret = FILAR_PagesIn(&fin);
        if (ret)
	  rcc_error_print(stdout, ret);
      }
      else
      {
        for(channel = 0; channel < MAXROLS; channel++)
        {
          if (finfo.channels[channel] && finfo.enabled[channel])
          {
            fin.channel = channel;
      
            for(loop = 0; loop < nelem; loop++)
              fin.pciaddr[loop] = pcibase + 0x1000 * (loop & 0x3);  //we use 4 single page buffers in a cyclic manner
      
            ret = FILAR_PagesIn(&fin);
            if (ret)
	      rcc_error_print(stdout, ret);
          }
        }
      }
    }
    
    if (fun == 6)
    {      
      printf("Enter the number of the channel (0..%d, %d=all): ", MAXROLS, MAXROLS + 1);
      achannel = getdecd((int)achannel);

      if (achannel < (MAXROLS + 1))
      {
        printf("test_filar: channel = %d \n", achannel);
            
        ret = FILAR_PagesOut(achannel, &fout);
        if (ret)
	  rcc_error_print(stdout, ret);
      
        printf("%d entries received\n", fout.nvalid);
        for(loop = 0; loop < fout.nvalid; loop++)
        {
          printf("Entry %d:\n", loop);
          printf(" PCI address     = 0x%08x\n", fout.pciaddr[loop]);
          printf(" Fragment size   = 0x%08x\n", fout.fragsize[loop]);
          printf(" Fragment status = 0x%08x\n", fout.fragstat[loop]);
        }
      }
      else
      {
        for(channel = 0; channel < MAXROLS; channel++)
        {
          if (finfo.channels[channel] && finfo.enabled[channel])
          {            
            ret = FILAR_PagesOut(channel, &fout);
            if (ret)
	      rcc_error_print(stdout, ret);
        
            printf("%d entries received from channel %d\n", fout.nvalid, channel);
            for(loop = 0; loop < fout.nvalid; loop++)
            {
              printf("Entry %d:\n", loop);
              printf(" PCI address     = 0x%08x\n", fout.pciaddr[loop]);
              printf(" Fragment size   = 0x%08x\n", fout.fragsize[loop]);
              printf(" Fragment status = 0x%08x\n", fout.fragstat[loop]);
            }
          }
        }
      }
    }
        
    if (fun == 7)
    {
      printf("This function will reset the card that owns the specified channel\n");
      printf("All other channels of that card will be reset too\n");
      printf("Which channel do you want to reset: ");
      achannel = getdecd((int)achannel);
      ret = FILAR_Reset(achannel);
      if (ret)
	rcc_error_print(stdout, ret);
    }  
    
    if (fun == 8) 
    {
      printf("Which channel do you want to probe: ");
      achannel = getdecd((int)achannel);

      ret = FILAR_InFree(achannel, &nfree);   
      if (ret)
	rcc_error_print(stdout, ret);
        
      printf("The IN FIFO of channel %d has %d free slots\n", achannel, nfree);
    }
    
    if (fun == 9) 
    {
      ret = FILAR_Flush();   
      if (ret)
	rcc_error_print(stdout, ret);        
      printf("Buffered packets flushed\n");
    }
    
    if (fun == 10) 
    {
      printf("Which channel do you want to reset: ");
      achannel = getdecd((int)achannel);
      
      ret = FILAR_LinkReset(achannel);   
      if (ret)
	rcc_error_print(stdout, ret);        
      printf("Link has been reset\n");
    }
    
    if (fun == 11) 
    {
      printf("Which channel do you want to test: ");
      achannel = getdecd((int)achannel);
      
      ret = FILAR_LinkStatus(achannel, &data);   
      if (ret)
	rcc_error_print(stdout, ret);        
      printf("The link of channel %d is %s\n", achannel, (data)?"up":"down");
    }
    
    
  }
  printf("=========================================================================\n\n");
  return(0);
}












