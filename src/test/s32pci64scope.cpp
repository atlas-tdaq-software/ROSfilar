/****************************************************************/
/*                                                              */
/*  file: s32pci64scope.c                                       */
/*                                                              */
/* This program allows to access the resources of a S32PIC64    */
/* card in a user friendly way and includes some test routines  */
/*                                                              */
/*  Author: Markus Joos, CERN-EP                                */
/*                                                              */
/*   2.05.01  MAJO  created                                     */
/*                                                              */
/****************C 2001 - A nickel program worth a dime**********/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "io_rcc/io_rcc.h"
#include "cmem_rcc/cmem_rcc.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"

#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

/*prototypes*/
void SigQuitHandler(int signum);
int s32pci64_map(void);
int s32pci64_unmap(void);
int dumpconf(void);
int gensc(void);
int dumpmem(void);
int setocrp(void);
int setreq(int mode);
int setrbs(void);
int setti(void);
int testin(void);
int setpci(void);
int setim(void);
int setreg(void);
int uio_init(void);
int uio_exit(void);
int getbuf(int mode);
int retbuf(int mode);
int readack(void);
int dumpbuff(void);
int cardreset(void);
int mainhelp(void);
int linkreset(void);
void SLIDASDataCheck1(unsigned int *bptr, int w);
void SLIDASDataCheck2(unsigned int *bptr, int w);
void SLIDASDataCheck3(unsigned int *bptr, int w);
void SLIDASDataCheck4(unsigned int *bptr, int w);
void SLIDASDataCheck8(unsigned int *bptr, int w);
int slidastest(void);
int loadpci(void);

/*constants*/
#define MAXBUF  30
#define BUFSIZE (16*1024)            /*bytes*/
// ELONEX: #define MEMADD  0xfebffc00           /*check with '/sbin/lspci -v'*/
#define MEMADD  0xfeafec00           /*check with '/sbin/lspci -v'*/
#define PREFILL 0xfeedbabe

/*types*/
typedef struct
{
unsigned int ocr;       /*0x000*/
unsigned int osr;       /*0x004*/
unsigned int imask;     /*0x008*/
unsigned int tin;       /*0x00c*/
unsigned int d1[60];    /*0x010-0x0fc*/
unsigned int rfa;       /*0x100*/
unsigned int rfl;       /*0x104*/
unsigned int d2[62];    /*0x108-0x1fc*/
unsigned int afs;       /*0x200*/
unsigned int afe;       /*0x204*/
unsigned int afl;       /*0x208*/
}T_s32pci64_regs;



/*globals*/
static unsigned int reqsize=(BUFSIZE>>2),cont,first_event,shandle;
static unsigned int sreg;
static T_s32pci64_regs *slink;
static unsigned int pcidefault[16]={
0x001210dc,0x00800000,0x02800000,0x0f0ef800,
0xfffffc00,0x00000000,0x00000000,0x00000000,
0x00000000,0x00000000,0x00000000,0x00000000,
0x00000000,0x00000000,0x00000000,0x000001ff};
static int bhandle[MAXBUF];
static u_int uaddr[MAXBUF], paddr[MAXBUF];
static int bfree=MAXBUF,firstbuf=0;
static struct sigaction sa;

/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
cont=0;
debug(("SigQuitHandler: ctrl+// received\n"));
}


/********************/
int s32pci64_map(void)
/********************/
{
unsigned int eret, pciaddr;

eret = IO_Open();
if (eret != IO_RCC_SUCCESS)
  {
  rcc_error_print(stdout, eret);
  exit(-1);
  }

eret = IO_PCIDeviceLink(0x10dc, 0x0012, 1, &shandle);
if (eret != IO_RCC_SUCCESS)
  {
  rcc_error_print(stdout, eret);
  exit(-1);
  }  

eret = IO_PCIConfigReadUInt(shandle, 0x10, &pciaddr);
if (eret != IO_RCC_SUCCESS)
  {
  rcc_error_print(stdout, eret);
  exit(-1);
  } 

 
eret = IO_PCIMemMap(pciaddr, 0x1000, &sreg);
if (eret != IO_RCC_SUCCESS)
  {
  rcc_error_print(stdout, eret);
  exit(-1);
  } 

slink=(T_s32pci64_regs *)sreg; 
return(0);
}


/**********************/
int s32pci64_unmap(void)
/**********************/
{
unsigned int eret;

eret = IO_Close();
if(eret)
  rcc_error_print(stdout,eret);
    
eret = IO_PCIMemUnmap(sreg, 0x1000);
if(eret)
  rcc_error_print(stdout,eret);
return(0);
}



/****************/
int dumpconf(void)
/****************/
{
unsigned int loop, eret, data;

printf("PCI configuration registers:\n\n");
printf("Offset  |  content  |  Power-up default\n");
for(loop=0;loop<0x40;loop+=4)
  {
  printf("   0x%02x |",loop);
        
  eret=IO_PCIConfigReadUInt(shandle, loop, &data);      
  if(eret)
    printf(" failed to read register\n");
  else
    {
    printf("   0x%08x |",data);
    if(data==pcidefault[loop>>2])
      printf(" Yes\n");
    else
      printf(" No (0x%08x)\n",pcidefault[loop>>2]);
    }
  }

return(0);
}


/*************/
int gensc(void)
/*************/
{
unsigned int loop,data;

for(loop=0;loop<10;loop++)
  data=slink->ocr;

for(loop=0;loop<10;loop++)
  slink->ocr=data;

for(loop=0;loop<10;loop++)
  {
  data=slink->ocr;
  slink->ocr=data;
  }

for(loop=0;loop<90000000;loop++)
  data=slink->ocr;

return(0);
}


/***************/
int dumpmem(void)
/***************/
{
unsigned int data, value;
  
data=slink->ocr;
printf("\nOperation Control register (0x%08x)\n",data);
printf("URL0-3..................0x%01x           User data width..",(data>>20)&0xf);
value=(data>>18)&0x3;
if(value==0) printf("..........32 bit\n");
if(value==1) printf("..........16 bit\n");
if(value==2) printf("...........8 bit\n");
if(value==3) printf("........reserved\n");
printf("URESET..................%s  UTDO.......................%s\n",(data&0x20000)?"set         ":"not set     ",(data&0x10000)?"set       ":"not set");
printf("Test mode UCL...........%s  Test mode..................%s\n",(data&0x20)   ?"control word":"data word   ",(data&0x10)?"enabled":"disabled");
printf("Stop request............%s  Swap double word (64-bit)..%s\n",(data&0x8)    ?"set         ":"not set     ",(data&0x4)?"yes":"no");
printf("Byte swap 32-bit words..%s  Reset interface............%s\n",(data&0x2)    ?"yes         ":"no          ",(data&0x1)?"set":"not set");

data=slink->osr;
printf("\nOperation Status register (0x%08x)\n",data);
printf("# words in input buffer..0x%02x       # words in PCI FIFO.........0x%02x\n",(data>>26)&0x3f,(data>>20)&0x3f);
printf("UXOFF....................%s    Overflow....................%s\n",(data&0x80000)?"set    ":"not set",(data&0x40000)?"yes":"no");
printf("Link down................%s        Stop acknowledge............%s\n",(data&0x20000)?"yes":"no ",(data&0x10000)?"yes":"no");
printf("# entries in ACK FIFO....%02d         # entries left in REQ FIFO..%d\n",(data>>8)&0xf,data&0xf);

data=slink->imask;
printf("\nInterrupt Mask register (0x%08x)\n",data);
printf("UXOFF interrupt..............%s  Overflow interrupt...........%s\n",(data&0x80000)?"enabled ":"disabled",(data&0x40000)?"enabled":"disabled");
printf("Link down interrupt..........%s  Stop ack. interrupt..........%s\n",(data&0x20000)?"enabled ":"disabled",(data&0x10000)?"enabled":"disabled");
printf("Limit for ACK_AVAILABLE IRQ..%02d        Limit for REQ_AVAILABLE IRQ..%02d\n",(data>>8)&0xf,data&0xf);

return(0);
}

/***************/
int setocrp(void)
/***************/
{
int data;

data=slink->ocr;
printf("Enter new value for the Operation Control register ");
data=gethexd(data);
slink->ocr=data;
return(0);
}


/******************/
int setreq(int mode)
/******************/
{
static int num=1;
int free,bufnr,data,loop;

if(mode)
  {
  printf("Enter the number of entries to be sent ");
  num=getdecd(num);
  }
else
  num=1;

data=slink->osr;
free=data&0xf;
if(free<num)
  {
  printf("The Request FIFO has only space for %d entries\n",free);
  return(2);
  }

for(loop=0;loop<num;loop++)
  {
  bufnr=getbuf(mode);
  if(bufnr<0)
    {
    printf("All memory buffers are in use\n");
    return(1);
    }

  slink->rfa=paddr[bufnr];
  slink->rfl=reqsize;
  if(mode)
    {
    printf("FIFO filled with PCI address=0x%08x, size=%d words\n",paddr[bufnr],reqsize);
    printf("-->buffer number is %d\n",bufnr);
    }
  }

return(0);
}


/**************/
int setrbs(void)
/**************/
{
static int bsize=0x400;

printf("Enter the request size in words: ");
bsize=gethexd(bsize);
if(bsize>(BUFSIZE>>2))
  {
  printf("Warning: Max. size = 0x%04x\n",BUFSIZE);
  reqsize=BUFSIZE;
  printf("Request size set to max. value\n");
  }
else
  reqsize=bsize;
return(0);
}


/*************/
int setti(void)
/*************/
{
int data,value=0x12345678;

data=slink->osr;
printf("# words in input buffer:  %d  # words in PCI FIFO:  %d\n",(data>>26)&0x3f,(data>>20)&0x3f);

printf("Enter new value for the Test Input register ");
value=gethexd(value);
slink->tin=value;

data=slink->osr;
printf("# words in input buffer:  %d  # words in PCI FIFO:  %d\n",(data>>26)&0x3f,(data>>20)&0x3f);

return(0);
}


/**************/
int testin(void)
/**************/
{
static unsigned int stacw=0xb0f00000, endcw=0xe0f00000, numwo=10;
unsigned int pattern, loop;

printf("Enter the start control word ");
stacw=gethexd(stacw);

printf("Enter the end control word ");
endcw=gethexd(endcw);

printf("Enter the number of data words ");
numwo=getdecd(numwo);

/*Reset the FIFOs*/
cardreset();

/*Fill the REQ FIFO with one entry*/
setreq(0);

/*Write the start control word*/
slink->ocr=0x30;
slink->tin=stacw;

/*Write the data*/
slink->ocr=0x10;
for(loop=0;loop<numwo;loop++)
  {
  pattern=1<<(loop%32);
  slink->tin=pattern;
  }

/*Write the end control word*/
slink->ocr=0x30;
slink->tin=endcw;

usleep(100);

/*Read the ACK FIFO*/
readack();

/*reset the control register*/
slink->ocr=0;

return(0);
}


/**************/
int setpci(void)
/**************/
{
unsigned int eret,offset=0,data;

printf("Enter the offset of the register (4-byte alligned) ");
offset=gethexd(offset);
offset&=0x3c;

eret=IO_PCIConfigReadUInt(shandle, offset, &data);      
if(eret)
  {
  printf(" failed to read register\n");
  return(1);
  }
printf("Enter new value for this register ");
data=gethexd(data);

eret=IO_PCIConfigWriteUInt(shandle, offset, data);      
if(eret)
  {
  printf(" failed to write register\n");
  return(2);
  }

return(0);
}


/*************/
int setim(void)
/*************/
{
int data;

data=slink->imask;
printf("Enter new value for the Interrupt Mask register ");
data=gethexd(data);
slink->imask=data;
return(0);
}



/**************/
int setreg(void)
/**************/
{
int fun=1;

printf("\n=========================================\n");
while(fun!=0)
  {
  printf("\n");
  printf("Select an option:\n");
  printf("   1 Operation Control register   2 Request FIFO\n");
  printf("   3 Test Input register          4 PCI config. register\n");
  printf("   5 Interrupt Mask register\n");
  printf("   0 Exit\n");
  printf("Your choice ");
  fun=getdecd(fun);
  if (fun==1) setocrp();
  if (fun==2) setreq(1);
  if (fun==3) setti();
  if (fun==4) setpci();
  if (fun==5) setim();
  }
printf("=========================================\n\n");
return(0);
}


/****************/
int uio_init(void)
/****************/
{
unsigned int *ptr,loop2,loop,eret;

eret = CMEM_Open();
if (eret)
  {
  printf("Sorry. Failed to open the cmem_rcc library\n");
  rcc_error_print(stdout,eret);
  exit(6);
  }
  
for(loop=0;loop<MAXBUF;loop++)
  {
  eret=CMEM_SegmentAllocate(BUFSIZE, "slink", &bhandle[loop]);
  if(eret)
    {
    printf("Sorry. Failed to allocate buffer #%d\n",loop+1);
    rcc_error_print(stdout,eret);
    exit(7);
    }
    
  eret=CMEM_SegmentVirtualAddress(bhandle[loop], &uaddr[loop]);
  if(eret)
    {
    printf("Sorry. Failed to get virtual address for buffer #%d\n",loop+1);
    rcc_error_print(stdout,eret);
    exit(8);
    }
  
  eret=CMEM_SegmentPhysicalAddress(bhandle[loop], &paddr[loop]);
  if(eret)
    {
    printf("Sorry. Failed to get physical address for buffer #%d\n",loop+1);
    rcc_error_print(stdout,eret);
    exit(9);
    }
   
  /*initialise the buffer*/
  ptr=(unsigned int *)uaddr[loop];
  for(loop2=0;loop2<(BUFSIZE>>2);loop2++)
    *ptr++=PREFILL;
  }
return(0);
}


/****************/
int uio_exit(void)
/****************/
{
unsigned int loop,eret;

for(loop=0;loop<MAXBUF;loop++)
  {
  eret=CMEM_SegmentFree(bhandle[loop]);
  if(eret)
    {
    printf("Warning: Failed to free buffer #%d\n",loop+1);
    rcc_error_print(stdout,eret);
    }
  }

eret=CMEM_Close();
if(eret)
  {
  printf("Warning: Failed to close the CMEM_RCC library\n");
  rcc_error_print(stdout,eret);
  }
return(0);
}


/******************/
int getbuf(int mode)
/******************/
{
int bufnr;

if(bfree==0)
  return(-1);
else
  {
  bfree--;
  bufnr=firstbuf;
  firstbuf++;
  if(firstbuf>(MAXBUF-1))
    firstbuf=0;
  if(mode)
    printf("Buffer %d allocated\n",bufnr);
  return(bufnr);
  }
}


/******************/
int retbuf(int mode)
/******************/
{
int bufnr;

bufnr=firstbuf-MAXBUF+bfree;
if(bufnr<0)
  bufnr+=MAXBUF;
bfree++;
if(mode==1)
  printf("Buffer %d returned\n",bufnr);
return(bufnr);
}


/***************/
int readack(void)
/***************/
{
unsigned int loop,bnum,*ptr,data,data2,data3;

data=(slink->osr>>8)&0xf;
if(!data)
  {
  printf("The ACK FIFO is empty\n");
  return(1);
  }
  
data=slink->afs;
data2=slink->afe;
data3=slink->afl;  //length has to be read last

printf("Raw data read from offset 0x200 = 0x%08x\n", data);
printf("Raw data read from offset 0x204 = 0x%08x\n", data2);
printf("Raw data read from offset 0x208 = 0x%08x\n", data3);

printf("Start control word present:  %s\n",(data&0x4)?"no":"yes");
printf("Start control word contents: %d\n",data&0x3);
printf("Start control word:          0x%08x\n",data&0xfffffff8);
printf("End control word present:    %s\n",(data2&0x4)?"no":"yes");
printf("End control word contents:   %d\n",data2&0x3);
printf("End control word:            0x%08x\n",data2&0xfffffff8);
printf("Block length (4-byte words): 0x%08x\n",data3);
bnum=retbuf(1);

/*Print the first 10 words of the event*/
ptr=(unsigned int *)uaddr[bnum];
for(loop=0;loop<10;loop++)
  printf("Word %d = 0x%08x\n",loop+1,*ptr++);

return(0);
}


/****************/
int dumpbuff(void)
/****************/
{
static int size=0x10,bnum=0;
unsigned int loop,*ptr;

printf("Enter the number of the buffer \n");
bnum=getdecd(bnum);
printf("Enter the number of words to be dumped \n");
size=getdecd(size);

ptr=(unsigned int *)uaddr[bnum];
for(loop=0;loop<size;loop++)
  printf("Offset=0x%04x  data=0x%08x\n",loop*4,*ptr++);

return(0);
}


/*****************/
int cardreset(void)
/*****************/
{
unsigned int data;

data=slink->ocr;
data|=0x1;
slink->ocr=data;
sleep(1);
data&=0xfffffffe;
slink->ocr=data;

/*reset the buffers*/
bfree=MAXBUF;
firstbuf=0;
return(0);
}


/*****************/
int linkreset(void)
/*****************/
{
/*set the URESET bit*/
slink->ocr|=0x20000;
ts_delay(10); /*to be sure. 1 us should be enough*/

/*now wait for LDOWN to come up again*/
printf("Waiting for link to come up...\n");
while(slink->osr&0x20000);

/*reset the URESET bit*/
slink->ocr&=0xfffdffff;
return(0);
}


/****************/
int mainhelp(void)
/****************/
{
printf("Call Markus Joos, 72364, 160663 if you need help\n");
return(0);
}


/**********************************************/
void SLIDASDataCheck1(unsigned int *bptr, int w)
/**********************************************/
{
static int lastlength=0;
unsigned int *ptr,data;

if(w!=lastlength && !first_event)
  printf("Packet has %d words (previous had %d words)\n", w, lastlength); 
lastlength=w;


data=1;
for(ptr=bptr;ptr<(bptr+w);ptr++)
  {
  if(*ptr!=data)
    {
    printf("ERROR: Received: 0x%08x  expected 0x%08x  offset: 0x%08x\n",*ptr,data,(unsigned int)ptr-(unsigned int)bptr); 
    sleep(1);
    }
  data=data<<1;
  if(data==0)
    data=1;
  }
}


/**********************************************/
void SLIDASDataCheck2(unsigned int *bptr, int w)
/**********************************************/
{
static int lastlength=0;
unsigned int *ptr,data;
  
if(w!=lastlength && !first_event)
  printf("Packet has %d words (previous had %d words)\n", w, lastlength);
lastlength=w;

data=0xfffffffe;
for(ptr=bptr;ptr<(bptr+w);ptr++)
  {
  if(*ptr!=data)
    printf("ERROR: Received: 0x%08x  expected 0x%08x  offset: 0x%08x\n",*ptr,data,(unsigned int)ptr-(unsigned int)bptr);
  data=(data<<1)|0x1;
  if(data==0xffffffff)
    data=0xfffffffe;
  }   
}


/**********************************************/
void SLIDASDataCheck3(unsigned int *bptr, int w)
/**********************************************/
{
static int lastlength=0;
unsigned int *ptr,data; 

if(w!=lastlength && !first_event)
  printf("Packet has %d words (previous had %d words)\n", w, lastlength);
lastlength=w;

  
data=0xffffffff;
for(ptr=bptr;ptr<(bptr+w);ptr++)
  {
  if(*ptr!=data)  
    printf("ERROR: Received: 0x%08x  expected 0x%08x  offset: 0x%08x\n",*ptr,data,(unsigned int)ptr-(unsigned int)bptr);
  if(data==0xffffffff)
    data=0;  
  else
    data=0xffffffff;
  } 
}  


/**********************************************/
void SLIDASDataCheck4(unsigned int *bptr, int w)
/**********************************************/
{
static int lastlength=0;
unsigned int *ptr,data; 

if(w!=lastlength && !first_event)
  printf("Packet has %d words (previous had %d words)\n", w, lastlength);
lastlength=w;


data=0xaaaaaaaa;
for(ptr=bptr;ptr<(bptr+w);ptr++)
  {
  if(*ptr!=data)  
    printf("ERROR: Received: 0x%08x  expected 0x%08x  offset: 0x%08x\n",*ptr,data,(unsigned int)ptr-(unsigned int)bptr);
  if(data==0xaaaaaaaa)
    data=0x55555555;
  else
    data=0xaaaaaaaa;
  } 
}   


/**********************************************/
void SLIDASDataCheck8(unsigned int *bptr, int w)
/**********************************************/
{ 
static unsigned int l1id=0, bcid=0;
static int lastlength=0;
unsigned int slidas_hdr_wrd[]={0xeeeeeeee, 0x00000020, 0x08000003, 0xa0000000, 0x1, 0x1, 0x2, 0x000000de};
unsigned int slidas_stat_wrd[]={0x0, 0x4, 0x0};
unsigned int slidas_trl_wrd[]={0x3, 0x0, 0x0};
const int stat_size=0x3,hdr_size=0x8,trl_size=0x3;
unsigned int i,*ptr;
int data_size; 
  
if(w!=lastlength && !first_event)
  printf("Packet has %d words (previous had %d words)\n", w, lastlength);
  
if (first_event)  
  l1id=bcid=0;
  
lastlength=w;
  
slidas_hdr_wrd[4]=l1id;
slidas_hdr_wrd[5]=bcid;
  
/* Check the header */
for(i=0,ptr=bptr;ptr<(bptr+hdr_size);ptr++,i++) 
  {
  if(*ptr!=slidas_hdr_wrd[i])
    printf("Header Word %d is %#x\texpected %#x (diff is %#x)\n",i, *ptr, slidas_hdr_wrd[i], (slidas_hdr_wrd[i]-*ptr));
  }

/* Check the status words */
ptr=bptr+hdr_size; 
for(i=0;ptr<(bptr+hdr_size+stat_size);ptr++,i++) 
  {
  if(*ptr!=slidas_stat_wrd[i])
    printf("Status word %d is %#x\texpected %#x (diff is %#x)\n",i, *ptr, slidas_stat_wrd[i], (slidas_stat_wrd[i]-*ptr));
  }

/* Check the data */
data_size=*(bptr+w-2);
ptr=bptr+hdr_size+stat_size;
for(i=0;ptr<(bptr+hdr_size+stat_size+data_size);ptr++,i++) 
  {
  if(*ptr!=i) 
    printf("Data word   %d is %#x\texpected %#x (diff is %#x)\n",i, *ptr, i, (i-*ptr));
  }          

/* Check the trailer */
i=0x0; 
ptr=bptr+hdr_size+stat_size+data_size;
if(*ptr!=slidas_trl_wrd[i])
  printf("Trailer word  %d is %#x\texpected %#x\n",i, *ptr, slidas_trl_wrd[i]);

i=0x1;
if(data_size!=(w-hdr_size-stat_size-trl_size))
  printf("Trailer word %d is %#x instead of %#x\n",i, data_size, (w-hdr_size-stat_size-trl_size));

i=0x2;
if(*(bptr+w-1)!=slidas_trl_wrd[i])
  printf("Trailer word %d is %#x instead of %#x\n",i, *(bptr+w-1), slidas_trl_wrd[i]);   

l1id=++l1id&0xffff;                 /*Slidas has a 16 bit L1ID counter*/
bcid=(bcid+3)&0xfff;              
first_event=0;
}


/***************/
int loadpci(void)
/***************/
{
unsigned int bnum,data,data2,data3,eret;

cont=1;
printf("Running! Press <ctrl+\\> when finished\n");
while(cont)
  {
  /*Write one address to the request FIFO*/
  eret=setreq(0);
  if(eret)
    {
    printf("Error %d received from setreq\n",eret);
    return(1);
    }

  /*Wait for a frqagment to arrive*/
  data=(slink->osr>>8)&0xf;
  while(!data)
    data=(slink->osr>>8)&0xf;

  /*Read the ACK fifo*/
  data=slink->afs;
  data2=slink->afe;
  data3=slink->afl;
  /*return the buffer and get a pointer to the data*/
  bnum=retbuf(0);
  }
return(0);
}


/******************/
int slidastest(void)
/******************/
{
static unsigned int scw=0xb1000000,ecw=0xe1000000,sw1=1,nol=1;
unsigned int rmode,size,ffrag,complete,ok=0,loop,bnum,*ptr,data,data2,data3,eret,evdata[0x10000];

printf("Enter the start control word: ");
scw=gethexd(scw);

printf("Enter the end control word: ");
ecw=gethexd(ecw);

printf("How many packets do you want to check? (0 = run forever)");
nol=getdecd(nol);
if (!nol)
  rmode=1;
else
  rmode=0;

while(!ok)
  {
  printf("Enter the value of SW1 on the SLIDAS ");
  sw1=getdecd(sw1);
  if(sw1!=1 && sw1!=2  && sw1!=3  && sw1!=4 && sw1!=8)
    printf("Sorry. Only positions 1,2,3,4 and 8 are supported\n");
  else
    ok=1;
  }

first_event=1;
cardreset();
linkreset();

cont=1;
if(rmode)
  printf("Running! Press <ctrl+\\> when finished\n");
loop=0;
while(cont)
  {
  if(loop==0)
    printf("Waiting for data...\n"); 
    
  /*Receive a complete, potentially fragmented packet*/
  complete=0;
  ffrag=1;
  size=0;
  
  while(!complete)
    {    
    /*Write one address to the request FIFO*/
    eret=setreq(0);
    if(eret)
      {
      printf("Error %d received from setreq\n",eret);
      return(1);
      }

    /*Wait for a frqagment to arrive*/
    data=(slink->osr>>8)&0xf;
    while(!data)
      data=(slink->osr>>8)&0xf;

    /*Read the ACK fifo*/
    data=slink->afs;
    data2=slink->afe;
    data3=slink->afl;
    
    if(loop==0)
      {
      printf("Start control word present: %s\n",(data&0x4)?"no":"yes");
      printf("Start control word contents: %d\n",data&0x3);
      printf("Start control word: 0x%08x\n",data&0xfffffff8);
      printf("End control word present: %s\n",(data2&0x4)?"no":"yes");
      printf("End control word contents: %d\n",data2&0x3);
      printf("End control word: 0x%08x\n",data2&0xfffffff8);
      printf("Block length (4-byte words): 0x%08x\n",data3);
      }

    /*Check the content of the ACK FIFO*/
    if(ffrag && data&0x4)
      printf("ERROR: Packet #%d has no start control word\n",loop);
    if(ffrag && (data&0xfffffff8)!=scw)
      printf("ERROR: Packet #%d has start control word 0x%08x\n",loop,data&0xfffffff8);
    if(data&0x3)
      printf("ERROR: Packet #%d has error %d in start control word\n",loop,data&0x3);
    if(data2&0x3)
      printf("ERROR: Packet #%d has error %d in end control word\n",loop,data2&0x3);      
    if(!(data2&0x4))
      {
      if((data2&0xfffffff8)!=ecw)
        printf("ERROR: Packet #%d has end control word 0x%08x\n",loop,data2&0xfffffff8);
      complete=1;
      }
    ffrag=0;

    /*return the buffer and get a pointer to the data*/
    bnum=retbuf(0);
    ptr=(unsigned int *)uaddr[bnum];
    
    /*Copy the data into the array*/
    if(data3)
      {
      if(loop==0)
        printf("Copying 0x%08x bytes\n",data3*4);
      memcpy(&evdata[size], ptr, data3*4);
      size+=data3;
      }
    }

  /*Check the data*/
  if(sw1==1) SLIDASDataCheck1(evdata, size);
  if(sw1==2) SLIDASDataCheck2(evdata, size);
  if(sw1==3) SLIDASDataCheck3(evdata, size);
  if(sw1==4) SLIDASDataCheck4(evdata, size);
  if(sw1==8) SLIDASDataCheck8(evdata, size);

  loop++;
  if(nol)
    {
    nol--;
    if(!nol)
      break;
    }
  }
return(0);
}


/************/
int main(void)
/************/
{
static int ret,fun=1;

sigemptyset(&sa.sa_mask);
sa.sa_flags = 0;
sa.sa_handler = SigQuitHandler;
ret=sigaction(SIGQUIT, &sa, NULL);
if(ret<0)
  {
  printf("Cannot install signal handler (error=%d)\n",ret);
  exit(0);
  }

ts_open(1,TS_DUMMY);
uio_init();
s32pci64_map();

while(fun!=0)
  {
  printf("\n");
  printf("Select an option:\n");
  printf(" 1 Print help           2 Dump PCI conf. space  3 Dump PCI MEM registers\n");
  printf(" 4 Write to a register  5 Read ACK FIFO         6 Reset the S32PCI64\n");
  printf(" 7 Reset the S-Link     8 Dump a buffer         9 Load PCI\n");
  printf("10 SLIDAS test         11 Set req. block size  12 Generate single cycles\n");
  printf("13 Use test input\n");
  printf(" 0 Quit\n");
  printf("Your choice ");
  fun=getdecd(fun);
  if (fun==1) mainhelp();
  if (fun==2) dumpconf();
  if (fun==3) dumpmem();
  if (fun==4) setreg();
  if (fun==5) readack();
  if (fun==6) cardreset();
  if (fun==7) linkreset();
  if (fun==8) dumpbuff();
  if (fun==9) loadpci();
  if (fun==10) slidastest();
  if (fun==11) setrbs();
  if (fun==12) gensc();
  if (fun==13) testin();
  }

s32pci64_unmap();
uio_exit();
ts_close(TS_DUMMY);
exit(0);
}







